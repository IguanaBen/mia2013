#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>


#define type unsigned long long

using namespace std;

const type sq_l = 1000000000ULL;

long long mulmod(long long a,long long b,long long c)
{
	if( a < sq_l && b < sq_l)
		return (a*b) % c;

    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

bool primess[100033];


type qpow(type q, type d, type n)
{
	if( d == 0)
		return 1;

	if(d % 2)
	{
		type t = qpow(q, d/2, n);
		return  mulmod(mulmod(t, t, n), q, n);
	}
	else
	{
		type t = qpow(q, d/2, n);
		return mulmod(t, t, n);
	}
}

type qpow_norm(type q, type d, type n)
{
	if( d == 0)
		return 1;

	if(d % 2)
	{
		type t = qpow(q, d/2, n);
		return  (t*t*q) % n;
	}
	else
	{
		type t = qpow(q, d/2, n);
		return (t*t) % n;
	}
}




type long_mult(type x, type y, type n)
{
	int num2 = 0;

	type res = 0;
	// type temp = x;
	while(y > 0)
	{
		if(y  % 2)
		{
			type temp = x;
			// type part = 0;
			for(int i=0; i<num2; i++)
			{
				temp *= 2;
				temp = temp % n;
			}
			res += temp;
			res %= n;
		}


		y /= 2;
		num2++;
	}

	return res;
}

bool miller_rabin_x(type n, int k)
{
	type s = 0;
	type d = n-1;

	while(d % 2 == 0)
	{
		d = d/2;
		s++;
	}

	for(int i=0; i<k; i++)
	{
		type a = rand()%(n-2) + 2;
		// cout<<a<<endl;
		type x = qpow(a, d, n);
		// cout<<a<<" "<<x<<" "<<n-1<<endl;
		if(!( x == 1 || x == n-1))
		{
			bool next = false;
			for(int j=0; j<s; j++)
			{
				x = mulmod(x, x, n);
				if(x == 1)
					return false;

				if(x == n-1)
				{
					next = true;
					break;
				}
			}

			if(!next)
				return false;
		}

	}

	return true;
}

bool miller_rabin(long long p,int iteration){
    if(p<2){
        return false;
    }
    if(p!=2 && p%2==0){
        return false;
    }
    long long s=p-1;
    while(s%2==0){
        s/=2;
    }
    for(int i=0;i<iteration;i++){
        long long a=rand()%(p-1)+1,temp=s;
        long long mod = 0;
        // long long mod= qpow(a, temp, p);
        if(p < 1000100)
    		mod = qpow_norm(a,temp,p);
    	else
    		mod = qpow(a, temp, p);

        while(temp!=p-1 && mod!=1 && mod!=p-1){
            mod=mulmod(mod,mod,p);
            temp *= 2;
        }
        if(mod!=p-1 && temp%2==0){
            return false;
        }
    }
    return true;
}

bool miller_rabin_small(long long p,int iteration){
    if(p<2){
        return false;
    }
    if(p!=2 && p%2==0){
        return false;
    }
    long long s=p-1;
    while(s%2==0){
        s/=2;
    }
    for(int i=0;i<iteration;i++){
        long long a=rand()%(p-1)+1,temp=s;
        long long mod = 0;
        // long long mod= qpow(a, temp, p);
    	mod = qpow_norm(a,temp,p);

        while(temp!=p-1 && mod!=1 && mod!=p-1){
            mod = (mod*mod) % p;
            temp *= 2;
        }
        if(mod!=p-1 && temp%2==0){
            return false;
        }
    }
    return true;
}

// bool miller_rabin(long long p,int iterations){
//     if(p == 1){ // 1 isn't prime
//         return false;
//     }
//     for(int i=0;i<iterations;i++){
//         // choose a random integer between 1 and p-1 ( inclusive )
//         long long a = rand()%(p-1)+1; 
//         // modulo is the function we developed above for modular exponentiation.
//         if(qpow(a,p-1,p) != 1){ 
//             return false; /* p is definitely composite */
//         }
//     }
//     return true; /* p is probably prime */
// }

// const type sq_l = 1000000000ULL;
const type lim = 1000000000000000000ULL;

const type ret = 18446744073709551615ULL;

type tab_limit[100];

type is_power(type n, type s, type e, int pw)
{	
	if(e-s > 1)
	{

		type candidate = (e-s)/2 + s;

		// if(candidate > tab_limit[pw])
		// 	return is_power(n, s, tab_limit[pw], pw);

		type res = candidate;
		// cout<<candidate<<endl;
		for(int j=0; j<pw-1; j++)
		{
			// if(res > lim)
			// 	return is_power(n, s, candidate, pw);

			if( res  < n)
				res *= candidate;
			else
				return is_power(n, s, candidate, pw);
		}

		// if(res > lim)
		// 	return is_power(n, s, candidate, pw);

		if(res <= 0)
			return is_power(n, s, candidate, pw);


		if( res <= n )
			return is_power(n, candidate, e, pw);
		else
			return is_power(n, s, candidate, pw); 

	}
	else
	{

		if(e-s == 0)
		{
			type candidate = s;
			type res = candidate;
			// cout<<candidate<<endl;

			for(int j=0; j<pw-1; j++)
			{
				if(res > n)
					return ret;
				// if(res > lim)
				// 	return ret;
				res *= candidate;

			}

			if(res == n)
				return candidate;
			else
				return ret;
		}

		if(e-s == 1)
		{
			// cout<<s<<" "<<e<<endl;
			type r1 = is_power(n,s,s,pw);
			type r2 = is_power(n,e,e,pw);

			if(r1 != ret)
				return r1;

			if(r2 != ret)
				return r2;

			return ret;
		}
	}

}

const int K = 2;

vector<int> primes;


type solve(type n)
{
	type orn = n;

	if( n == 1)
		return 0;

	if(n == 2)
		return 1;

	if( n == 4)
		return 3;

	if( n % 4 == 0)
		return 1;

	if(n % 2 == 0)
		n /= 2;

	// cout<<n<<endl;
	// cout<<miller_rabin(29, 10)<<endl;

	if( n < 100000)
	{
		if(primess[n])
			return orn-1;
	}
	else
	{
		if( miller_rabin(n, K) )
			return orn-1;
	}

	for(int i = 2; i < 5 ; i++)
	{
		// cout<<"Sd";
		type r = is_power(n, 3, tab_limit[i], i);
		// n = r; 
		// cout<<r<<endl;

		if( r != ret)
		{
			if( r < 100000)
			{
				if( primess[r])
					return orn-1;
			}
			else
			{
				if( miller_rabin(r, K))
					return orn-1;
			}
		}
	}





	// type cp5 = n;
	// while(cp5 % 5 == 0)
	// 	cp5/=5;

	// if( cp5 == 1)
	// 	return orn-1;

	// cp5 = n;
	// while(cp5 % 3 == 0)
	// 	cp5/=3;

	// if( cp5 == 1)
	// 	return orn-1;

	// cp5 = n;
	// while(cp5 % 7 == 0)
	// 	cp5/=7;

	// if( cp5 == 1)
	// 	return orn-1;

	type cp5 = n;
	int divis = 0;
	type last_divis = -1;
	for(int i=1; i< primes.size();)
	{

		if(cp5 % primes[i] == 0)
		{
			cp5 /= primes[i];
			if( last_divis != primes[i])
			{
				last_divis = primes[i];
				divis++;
				if(divis > 1)
					return 1;
			}
		}
		else
			i++;

	}

	if( cp5  == 1)
		return orn-1;

	return 1;


}

void init_limit()
{
	tab_limit[2] = 1000000000ULL;
	tab_limit[3] = 1000100;
	tab_limit[4] = 31623;
	tab_limit[5] = 3982;
	tab_limit[6] = 1000;
	tab_limit[7] = 372;
	tab_limit[8] = 177;
	tab_limit[9] = 100;
	tab_limit[10] = 64;
	tab_limit[11] = 43;
	tab_limit[12] = 32;
	tab_limit[13] = 25;
	tab_limit[14] = 20;
	tab_limit[15] = 16;
	tab_limit[16] = 14;
	tab_limit[17] = 12;
	tab_limit[18] = 10;
	tab_limit[19] = 9;
	tab_limit[20] = 7;
	tab_limit[21] = 7;
	tab_limit[22] = 6;
	tab_limit[23] = 6;

	for(int i=24; i<100; i++)
		tab_limit[i] = 7;
}


void small_p()
{
	

	primes.push_back(2);
	primes.push_back(3);


	//2!!
	for(int i = 5; i< 100000; i+=2)
	{
		bool p = true;
		for(int j=0; j< primes.size() && j < sqrt(i); j++)
		{
			if(i % primes[j] == 0)
			{
				p = false;
				break;
			}
		}

		if(p)
			primes.push_back(i);

	}

	for(int i =0; i < primes.size(); i++)
		primess[ primes[i]] = true;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  srand (time(NULL));
  init_limit();

  primes.clear();
  small_p();

  int T;
  cin>>T;

  for(type i=0; i<T; i++)
  {
  	type n;
  	cin>>n;
  	cout<<solve(n)<<"\n";


  }
  return 0;
}



