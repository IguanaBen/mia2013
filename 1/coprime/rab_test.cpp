#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>

#define type long long

using namespace std;

long long mulmod(long long a,long long b,long long c){
    long long x = 0,y=a%c;
    while(b > 0){
        if(b%2 == 1){
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

type qpow(type q, type d, type n)
{
	if( d == 0)
		return 1;

	if(d % 2)
	{
		type t = qpow(q, d/2, n);
		return  mulmod(mulmod(t, t, n), q, n);
	}
	else
	{
		type t = qpow(q, d/2, n);
		return mulmod(t, t, n);
	}
}

bool Fermat(long long p,int iterations){
    if(p == 1){ // 1 isn't prime
        return false;
    }
    for(int i=0;i<iterations;i++){
        // choose a random integer between 1 and p-1 ( inclusive )
        long long a = rand()%(p-1)+1; 
        // modulo is the function we developed above for modular exponentiation.
        if(qpow(a,p-1,p) != 1){ 
            return false; /* p is definitely composite */
        }
    }
    return true; /* p is probably prime */
}

bool Miller(long long p,int iteration){
    if(p<2){
        return false;
    }
    if(p!=2 && p%2==0){
        return false;
    }
    long long s=p-1;
    while(s%2==0){
        s/=2;
    }
    for(int i=0;i<iteration;i++){
        long long a=rand()%(p-1)+1,temp=s;
        long long mod=qpow(a,temp,p);
        while(temp!=p-1 && mod!=1 && mod!=p-1){
            mod=mulmod(mod,mod,p);
            temp *= 2;
        }
        if(mod!=p-1 && temp%2==0){
            return false;
        }
    }
    return true;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  srand (time(NULL));
  // init_limit();
  cout<<Miller(77777677777*2, 100)<<endl;
  cout<<Miller(77777677777, 100)<<endl;


  return 0;
}