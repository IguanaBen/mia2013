#include <iostream>
#include <string>
#include <vector>
#include <queue>

using namespace std;

vector<int> graph[100033];
vector<int> graph_o[100033];

int dist[100033];
int succ[100033];


void gen_succ(int n)
{
  int current = 1;
  int c_dist = dist[1] - 1;
  while(current != n)
  {
    // cout<<"SD";
    int next = 1000033; 
    for(int i = 0; i< graph_o[current].size(); i++)
    {
      // cout<<dist[graph_o[current][i]]<<" "<<c_dist<<endl;
      if( dist[graph_o[current][i]] == c_dist)
      {
        // cout<<graph_o[current][i]<<endl;
        if(next > graph_o[current][i])
          next = graph_o[current][i];
      }
    }
    c_dist--;
    cout<<current<<" ";
    current = next;
  }
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int T;
  cin>>T;

  for(int t=0; t<T; t++)
  {

    int n;
    cin>>n;

    for(int i=1; i<=n; i++)
    {
      graph[i].clear();
      graph_o[i].clear();
      dist[i] = -1;
    }

    for(int i =1; i<n; i++)
    {
      dist[i] = -1;
      int m;
      cin>>m;
      for(int j=0; j<m; j++)
      {
        int t;
        cin>>t;
        graph_o[i].push_back(t);
        graph[t].push_back(i);
      }
    }

    // for(int i =1; i<=n; i++)
    // {
    //   for(int j=0; j<graph[i].size(); j++)
    //   {
    //     cout<<graph[i][j]<<" ";
    //   }
    //   cout<<endl;
    // }

    dist[n] = -1;

    dist[n] = 0;

    queue<int> q;
    q.push(n);

    while(!q.empty())
    {
      int t = q.front();
      q.pop();

      for(int i=0; i<graph[t].size(); i++)
      {

        if(dist[graph[t][i]] == -1)
        {
          // cout<<t<<" "<<graph[t][i]<<endl;
          dist[graph[t][i]] = dist[t] + 1;
          q.push(graph[t][i]);
        }
      }
    }
    
    if(dist[1] != -1)
    {
      cout<<dist[1]<<"\n";
      // cout<<dist[1]<<" "<<dist[2]<<" "<<dist[3]<<" "<<dist[4]<<"\n";
      gen_succ(n);
      cout<<"\n";
    }
    else
    {
      cout<<-1<<"\n";
    }
  }

  return 0;
}