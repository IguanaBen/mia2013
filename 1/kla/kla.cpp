#include <iostream>
#include <string>
 
using namespace std;


int pi[1000033];
int q[1000033];


void compute_prefix(string p)
{
  int m = p.length()-1;
  pi[1] = 0;
  int k = 0;
  for(int q = 2; q<=m; q++)
  {
    while( k > 0 && p[k+1]!= p[q])
        k = pi[k];
    if ( p[k+1] == p[q])
      k = k+1;
    pi[q] = k;
  }
}

int kmp_match(string str, string p)
{
  int matches = 0;
  int n = str.length()-1;
  int m = p.length()-1;
  int q = 0;
  for(int i=1; i<=n; i++)
  {
    while(q>0 && p[q+1] != str[i])
      q = pi[q];

    if(p[q+1] == str[i])
      q = q+1;

    if(q == m)
    {
      matches++;
      q = pi[q];
    }
  }

  return matches;
}

 
int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  string str;
  string pattern;

  int T;
  cin>>T;

  for(int t =0; t<T; t++)
  {

    cin>>str;
    cin>>pattern;
    str = "#" + str;
    pattern = "#" + pattern;

    int sum_match = 0;
    bool is_q = false;
    for(int i=0; i<pattern.length(); i++ )
      if(pattern[i] == '?')
        is_q = true;

    if( is_q)
    {
      for(int i=97; i<97+26;i++)
      {

        string pat2 = pattern;
        for(int j=0; j<pat2.length(); j++)
        {
          if(pat2[j] == '?')
          {
            pat2[j] = i;
            break;
          }
        }
       compute_prefix(pat2);
       sum_match += kmp_match(str, pat2);
      }
    }
    else
    {
      compute_prefix(pattern);
      sum_match += kmp_match(str, pattern);
    }

    cout<<sum_match<<"\n";
  }


  return 0;
}