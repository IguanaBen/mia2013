#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;


type check_max(type x, int c)
{
	type res = x;

	for(int i=0; i<c; i++)
		res = res*4 - 1;
	return res;
}

type check_min(type x, int c)
{
	type res = x;

	for(int i=0; i<c; i++)
		res = res*4 - 1;
	return res;
}

type pow2(type d)
{
	if(d == 0)
		return 1;

	return 2*pow2(d-1);
}

type zfind(type x, type z, int d)
{
	if(d == 0)
		return 0;

	type left = check_max(x*4-3, d-1);
	type right = check_min(x*4-1, d-1);
	// cout<<left<<" "<<right<<" "<<x<<"\n";
	if(z < left)
		return zfind(x*4-3, z, d-1);
	else
		return pow2(d-1) + zfind(x*4-1, z, d-1);

}

type solve(type n)
{
	if(n < 3)
		return 0;

	type sum = 1;
	type pw = 2;
	type lmax = 3;

	int count = 1;

	while( lmax*4-1 <= n)
	{
		sum += pw;
		pw *= 2;
		lmax = lmax*4 - 1;
		count++;
	}

	// cout<<(sum + 1)/2<<" "<<pw-1<<"\n";

	// type rem

	return sum + zfind(3, n, count);
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type n;
  cin>>n;
  cout<<solve(n)<<"\n";

  // cout<<solve(1000000000)<<"\n";
  // cout<<solve(5)<<"\n";
  // cout<<solve(12)<<"\n";
  // cout<<solve(172)<<"\n";
  // cout<<zfind(3, 169, 3)<<"\n";
  // cout<<check_min(11, 2)<<"\n";

  return 0;
 
}