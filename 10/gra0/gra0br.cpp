#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long
 
using namespace std;


bool tab[1000];

bool check(int x, int k, bool r)
{

	if(x+1 >= k)
	{
		if(r)
			return false;
		else
			return true;
	}
	

	if( x *2 >=k)
		return check(x+1, k, !r);

	if( r == false)
		return check(x*2, k, !r) && check(x+1, k, !r);
	else
		return check(x*2, k, !r) || check(x+1, k, !r);

	// if( r == true)
	// 	return check(x*2, k, !r) || check(x+1, k, !r);
	// else
	// 	if(x*2 < k)
	// 		return check(x*2, k, !r) && check(x+1, k, !r);
	// 	else
	// 		return check(x+1, k, !r);
}

type tabx[10000000];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  // tab[0]=0;
  // tab[1]=0;
  // tab[2]=0;

  // for(int i=3; i<1000;i++)
  // {
  // 	if(i % 2)
  // 		tab[i] = !tab[i-1];
  // 	else
  // 	{
  // 		tab[i] = !tab[i-1] | !tab[i/2];
  // 	}
  // }

  // for(int i=1; i<200;i++)
  // {	
  // 	if(check(1,i,1))
  // 		cout<<i<<" "<<check(1, i, 1)<<"\n";
  // }

  tabx[1] = 3;

  for(int i=2; i<10000000;i++)
  {
  	if(i%2 == 0)
  		tabx[i] = tabx[i/2]*4 - 3;
  	else
  		tabx[i] = tabx[i/2]*4 - 1;
  }

  cout<<tabx[10000000-1]<<"\n";

  // cout<<"\n";

  return 0;
 
}