#include <iostream>
#include <cmath>
#include <fstream>

#define type long long

using namespace std;

const int limit = 10000000;
bool sieve[limit];
int primes[(limit/2)+1];
int root = ceil(sqrt(limit));

int main (int argc, char* argv[])
{
	//Create the various different variables required
	int insert = 2;
	primes[0] = 2;
	primes[1] = 3;
	for (int z = 0; z < limit; z++) sieve[z] = true;
	for (int x = 2; x <= root; x++)
	{
		if(sieve[x])
		{
			type val = x+x;
			while(val < limit)
			{
				sieve[val] = false;
				val+=x;
			}
		}
	}
	for (int a = 5; a < limit; a++)
	{
		if (sieve[a])
		{
			// primes[insert] = a;
			insert++;
		}
	}
	cout<<insert<<"\n";
	// ofstream file;
	// char filename[100];
	// sprintf(filename, "err_%d.txt", limit);
	// file.open(filename);
	// for (int a = 0; a < insert; a++) file << primes[a] << ((a == insert-1) ? "" : "\n");
	// file.close();
	// cout << "Written to file.\n";
	return 0;
}