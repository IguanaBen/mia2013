#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

vector<type> primes;

vector<type> vp[9];

// type mx = 10000000;


void gen_2(vector<type> &v, type mx)
{
	for(int i=0; i<503; i++)
	{
		for(int j=i+1; j<primes.size(); j++)
		{
			type val = primes[i] * primes[j];
			if(val > mx)
				break;
			else
				v.push_back(val);
		}
	}
}

void gen_3(vector<type> &v, type mx)
{

	for(int i=0; i<52; i++)
	{
		for(int j=i+1; j<primes.size(); j++)
		{
			for(int k=j+1; k<primes.size(); k++)
			{
				type val = primes[i] * primes[j] * primes[k];
				if(val > mx)
					break;
				else
					v.push_back(val);
			}

			if( primes[i]* primes[j]* primes[j+1] > mx)
				break;
		}
	}
}
// void gen_2(vector<type> &v, type mx)
// {
// 	for(int i=0; i<primes.size(); i++)
// 	{
// 		for(int j=i+1; j<primes.size(); j++)
// 		{
// 			type val = primes[i] * primes[j];
// 			if(val > mx)
// 				break;
// 			else
// 				v.push_back(val);
// 		}
// 	}
// }

void find_divs(vector<type> &v, type i, type c, type val, type mx)
{
	// cout<<primes.size()<<"\n";
	// cout<<i<<"\n";

	// if(i > 100000 && val != 1)
	// 	return;
	// cout<<v.size()<<" "<<val<<" "<<c<<"\n";


	if(c == 0 && val <=mx)
	{
		// if(val != 1)
		// cout<<val<<"\n";
		v.push_back(val);
		return;
	}

	if( val*primes[i] > mx )
		return;

	type vv = val;

	for(int x=0; x<c; x++)
	{
		vv*= primes[i];
		if(vv > mx )
			return;
	}

	// if( val*primes[i] > mx )
		// return;

	// if( i > 100)
		// return;	

	if(i == primes.size())
		return;

	// if(val * primes[i] > mx)
	// 	return;

	// if(i > 501 && c > 0 && primes.size()>501)
	// 	if(primes[501] < val)
	// 		return;

	// if(i > 50 && c > 1 && primes.size()>50)
	// 	if(primes[50] < val)
	// 		return;


	// if(i > 501 && c > 0)
	// 		return;

	// if(i > 50 && c > 1 )
	// 		returns;

	find_divs(v, i+1, c, val, mx);
	find_divs(v, i+1, c-1, val*primes[i], mx);

	// for(int j=1; j+i < 502; j++)
	// 	if(val*primes[j] <= mx )
	// 		find_divs(v, i+j, c-1, val*primes[j]);
	// 	else
	// 		break;

}


const int limit = 10000000;
bool sieve[limit];
// int primes[(limit/2)+1];

int main()
{
	// mx = mx*mx;

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  primes.reserve(700000);


  type n;
  cin>>n;

  type mx = sqrt(n);
  type limit = mx+3;
  type root = ceil(sqrt(limit));

 //  int sq = sqrt(mx);
 //  for(int i=5; i<=mx + 4 || i<=1000000; i+=2)
 //  {
 //  	if( i % 3 && i%5 )
 //  	{
 //  		bool p = true;
	//   	for(int j=0; j<primes.size() && primes[j] <= sq+4; j++)
	//   	{
	//   		if( i % primes[j]== 0)
	//   		{
	//   			p = false;
	//   			break;
	//   		}
	//   	}
	//   	if(p)
	//   		primes.push_back(i);
	// }
 //  }
	primes.push_back(2);
	primes.push_back(3);
	for (int z = 0; z < limit; z++) sieve[z] = true;
	for (int x = 2; x <= root; x++)
	{
		if(sieve[x])
		{
			type val = x + x;
			while(val < limit)
			{
				sieve[val] = false;
				val+=x;
			}
		}
	}
	for (int a = 5; a < limit; a++)
	{
		if (sieve[a])
		{
			primes.push_back(a);
		}
	}

  // cout<<primes.size()<<"\n";
  // cout<<primes[primes.size()-1]<<"\n";
  // cout<<primes[50]*primes[50]*primes[50]<<"\n";





	for(int j=3; j<8; j++)
	{
	  for(int i=0; i<primes.size(); i++)
	  	find_divs(vp[j], i+1, j, primes[i], mx);
	}

	 for(int i=0; i<primes.size(); i++)
	  	find_divs(vp[0], 1, 0, primes[i], mx);

	  gen_2(vp[1], mx);
	  gen_3(vp[2], mx);


	// for(int i=0; i<primes.size(); i++)
	// 	cout<<primes[i]<<"\n";
	// cout<<"\n";
	// cout<<vp[0].size()<<"\n";
	// cout<<"\n";

	type bb = 0;
	// for(int i=1; i<=n; i++)
	// {
	// 	for(int j=0; j<primes.size(); j++)
	// 	{
	// 		if(i % (primes[j]*primes[j]) == 0)
	// 		{
	// 			bb++;
	// 			break;
	// 		}
	// 	}
	// }

	// cout<<vp[1][0]<<"\n";
	int sgn = 1;

	type sum = 0;
	for(int j=0; j<8; j++)
	{
		// cout<<vp[j].size()<<"\n";
	  for(int i=0; i<vp[j].size(); i++)
	  {
	  	// cout<<vp[j][i]*vp[j][i]<<"\n";
	  	sum += (n/(vp[j][i] * vp[j][i]))*sgn;
	  }
	  sgn = sgn*(-1);
	}

	cout<<n-sum<<"\n";
	// cout<<n-bb<<"\n";
  // cout<<vp[0].size()<<" "<<primes.size()<<"\n";
  // for(int i=0; i< vp[0].size(); i++)
  	// cout<<vp[0][i]<<"\n";
  // vp[7].push_back(9699690);

  return 0;
 
}