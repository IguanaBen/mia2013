#include <iostream>

using namespace std;

int pos[1000042];

struct node
{
	int l, r;
	int val;
	int max_i;
};

const int MAX_N = 327680;
const int MAX_Q = 2*10042;
const int LG = 20;
const int max = 1000000099;
node tree[MAX_N+(MAX_Q*(LG+1)) + 42];


int last_free = MAX_N*2;
int root_indices[MAX_Q];

void init_tree()
{

	for(int i=MAX_N;i<MAX_N*2;i++)
	{
		node temp =   { -1, -1, 0,  i - MAX_N };
		tree[i] = temp;
	}

	for(int i=MAX_N-1; i>0;i--)
	{
		node temp =   { i*2, i*2+1,  tree[i*2+1].val + tree[i*2+1].val  , tree[i*2 + 1].max_i };
		tree[i] = temp;
	}


	last_free = MAX_N*2;
}
,
int update_tree(int old_index, int val, int s_index)
{
	if(tree[old_index].l == -1)
	{
		tree[last_free] = tree[old_index];
		tree[last_free].val = val;
		return last_free++;
	}

	if(s_index <= tree[ tree[old_index].l ].max_i)
	{
		int left = update_tree(tree[old_index].l, val, s_index);	
		tree[last_free] = tree[old_index];
		tree[last_free].l = left;
	}
	else
	{
		int right = update_tree(tree[old_index].r, val, s_index);	
		tree[last_free] = tree[old_index];
		tree[last_free].r = right;
	}

	tree[last_free].val = tree[tree[last_free].l].val + tree[tree[last_free].r].val;

	return last_free++;
}

int update(int index, int val, int root, int tab_index)
{
	if(pos[val] != -1)
	{
		int temp_root = update_tree(root, 0, pos[val] );
		pos[val] = index;
		return update_tree(temp_root, tab_index, index );
	}
	else
	{
		pos[val] = index;
		return update_tree(root, tab_index, index );

	}
}


int query_tree(int node_index, int index)
{

	if(tree[node_index].l == -1)
		return tree[node_index].val;

	if( index <= tree[ tree[node_index].l ].max_i )
	{
		return query_tree( tree[node_index].l , index);
	}
	else
	{
		return /*tree[ tree[node_index].l ].val + */query_tree( tree[node_index].r , index);
	}
}

int query(int i, int j)
{
	int sum_m = 0, sum_p = 0;
	if(i > 0)
		sum_m = query_tree(root_indices[j] ,i-1);

	sum_p = tree[root_indices[j]].val;

	return sum_p - sum_m;
}


int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	for(int i=0;i<1000042;i++)
		pos[i] = -1;

	init_tree();

	int n;
	cin>>n;


	// int tab[]  = { 1, 4, 4, 4, 3, 6, 7};
	int r = 1, t;
	for(int i=0; i<n;i++)
	{
		cin>>t;
		r = update(i, t, r);
		// cout<<r<<" "<<tree[r].val<<endl<<endl;
		root_indices[i] = r;
	}

	int q;
	cin>>q;
	int s, e;
	for(int i=0;i<q;i++)
	{
		cin>>s>>e;
		cout<<query(s-1,e-1)<<"\n";
	}

	// cout<<query(0,6);
	return 0;
}
