#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;


int tree[250000*2 + 3];

int last_lvl = 262144;
int root = 1;

int update(int index);

int find(int b, int index)
{
	cout<<index<<" "<<tree[index] <<"\n";
	if(index*2 >= last_lvl)
	{
	if( b <= tree[index*2])
		return index*2 - last_lvl;
	else
		return index*2  - last_lvl + 1;
	}

	if( b <= tree[index*2])
		return find(b, index*2);
	else
		return find(b-tree[index*2], index*2 +1 );
}

int set(int x, int index)
{
	int ind = last_lvl + index;
	tree[ind] = x;
	update(ind/2);
}

int update(int index)
{
	while(index > 0)
	{
		// cout<<index<<"\n";
		tree[index] = tree[index*2] + tree[index*2+1];
		index/=2;
	}

}

int ttab[250033];
int ntab[250033];

pair<int, int> tab[250033];

struct query
{
	int a, b, c, in;
};

query qq[15033];

bool cmppp(pair<int, int > p1, pair<int, int> p2)
{
	return p1.first < p2.first;
}

bool cmpq(query q1, query q2)
{
	return q1.a < q2.a;
}

int q_res[15033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  {
  	int t;
  	cin>>t;

  	ttab[i] = t;
  	tab[i].first = t;
  	tab[i].second = i;
  }

  sort(ttab, ttab + n, cmppp);
  for(int i=0; i<n; i++)
  	ntab[i] = ttab[i].second;


  for(int i=0; i<n; i++)
  	set(1, i);

  int q;
  cin>>q;

  for(int i=0; i<q; i++)
  {
  	int a,b,c, in;
  	cin>>a>>b>>c;
  	qq[i].a = a;
  	qq[i].b = b;
  	qq[i].c = c;
  	qq[i].in = i;
  }

  sort(qq ,qq +q, cmpq);

  int rem = 1;

  for(int i=0; i<q; i++)
  {
  	int d = qq[i].a - rem;
  	for(int j=0; j<d; j++)
  	{
  		set(0, rem-1 + j);
  	}
  	cout<<find(qq[i].b);
  	// q_res[qq[i].in] = 
  	rem += d;
  }

  return 0;
 
}