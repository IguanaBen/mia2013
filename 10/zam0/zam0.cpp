#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int N = 7004;

map<int, type> graph[7000];
typedef map<int, type>::iterator iter;
// int n;

type field[54][54];
const type infty = 1000000003;

int index(int a, int b)
{
	return a+b*52;
}

int bfs(int s, int t) {
        queue<pair<int, type> > q;
        type visit[N];
        fill(visit,visit+N,0);
        q.push(make_pair(s, 0));
        while (!q.empty()) {
                int tmp = q.front().first;
                type level = q.front().second;
                if (tmp == t) return level;
                q.pop();
                if (!visit[tmp]) {
                        visit[tmp] = 1;
                        for (map<int, type>::iterator it = graph[tmp].begin();
                                        it != graph[tmp].end(); it++)
                        {
                                if (it->second > 0 && !visit[it->first])
                                        q.push(make_pair(it->first, level + 1));
                        }
                }
        }
        return 0;
}
 
int dfs(int s, int t, int lv, vector<int> *cf) {
        int tmp;
        if (s == t) {
                type min = infty;
                for (vector<int>::iterator it = cf->begin(); it != cf->end(); it++)
                        if (min > *it) min = *it;
                return min;
        }
        if (lv == 0) return 0;
        for (iter it = graph[s].begin(); it != graph[s].end(); it++) {
                cf->push_back(it->second);
                if (it->second > 0 && (tmp = dfs(it->first, t, lv - 1, cf))) {
                        it->second -= tmp;
                        if (graph[it->first].find(s) != graph[it->first].end()) graph[it->first].insert(
                                        make_pair(s, tmp));
                        else graph[it->first][s] -= tmp;
                        return tmp;
                }
                cf->pop_back();
        }
        return 0;
}
 
int augment(int s, int t, int lv) {
        int sum = 0;
        int tmp;
        while ((tmp = dfs(s, t, lv, new vector<int>))) {
                sum += tmp;
        }
        return sum;
}
 
int maxflow(int s, int t) {
        int sum = 0;
        int level;
        while ((level = bfs(s, t))) {
                sum += augment(s, t, level);
        }
        return sum;
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
int n,m;

  cin>>n>>m;
  for(int i=1; i<=n; i++)
  	for(int j=1; j<=m; j++)
  		cin>>field[i][j];

  int e = 0;
  int s = 6999;


  for(int i=0; i<=n+2; i++)
  {
  	graph[s].insert(make_pair(  index(i, 0), infty) );

  	graph[  index(i, 0) ].insert(make_pair(  index(i, 0) + 3001, infty) );


  	graph[s].insert(make_pair(  index(i, m+1), infty) );
  	graph[  index(i, m+1) ].insert(make_pair(  index(i, m+1) + 3001, infty) );

  }

   for(int i=0; i<=m+2; i++)
  {
  	graph[s].insert(make_pair(  index(0, i), infty) );
  	graph[  index( 0, i) ].insert(make_pair(  index( 0, i) + 3001, infty) );


  	graph[s].insert(make_pair(  index(n+1, i), infty) );
  	graph[  index( n+1, i)].insert(make_pair(  index(n+1, i) + 3001, infty) );

  }



  for(int i=1; i<=n; i++)
  	for(int j=1; j<=m; j++)
  	{
  		// cout<<"Sdsdd\n";
  		int ind1 = i + j*52;
  		int ind2 = i + j*52 + 3001;

  		if(field[i][j] == -1)
  		{
  			e = ind2;
  			graph[ind1].insert(make_pair( ind2, infty) );
  		}
  		else
  		{
  			graph[ind1].insert(make_pair( ind2, field[i][j]) );

  			graph[ind2].insert(make_pair(  index(i-1, j), infty) );
  			graph[ind2].insert(make_pair(  index(i+1, j), infty) );
  			graph[ind2].insert(make_pair(  index(i, j-1), infty) );
  			graph[ind2].insert(make_pair(  index(i, j+1), infty) );

  		}

  		// cout<<index(i-1, j)
  		graph[index(i-1, j) + 3001].insert(make_pair( ind1, infty) );
  		graph[index(i, j-1) + 3001].insert(make_pair( ind1, infty) );
  		graph[index(i+1, j) + 3001].insert(make_pair( ind1, infty) );
  		graph[index(i, j+1) + 3001].insert(make_pair( ind1, infty) );



  	}

    type res = maxflow(s, e);
    if(res >= infty)
    	cout<<"NIE\n";
    else
    	cout<<res<<"\n";

  return 0;
 
}