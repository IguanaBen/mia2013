#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <string.h>

#define type long long
#define oo 1000000000 // Infinity

using namespace std;


type field[54][54];
const type infty = 1000000003;

int index(int a, int b)
{
  return a+b*51;
}


const int N = 5505;


const int INF = 2000000000;

typedef long long LL;

struct Edge {
  int from, to, cap, flow, index;
  Edge(int from, int to, int cap, int flow, int index) :
    from(from), to(to), cap(cap), flow(flow), index(index) {}
};

struct PushRelabel {
  int N;
  vector<vector<Edge> > G;
  vector<LL> excess;
  vector<int> dist, active, count;
  queue<int> Q;

  PushRelabel(int N) : N(N), G(N), excess(N), dist(N), active(N), count(2*N) {}

  void AddEdge(int from, int to, int cap) {
    G[from].push_back(Edge(from, to, cap, 0, G[to].size()));
    if (from == to) G[from].back().index++;
    G[to].push_back(Edge(to, from, 0, 0, G[from].size() - 1));
  }

  void Enqueue(int v) { 
    if (!active[v] && excess[v] > 0) { active[v] = true; Q.push(v); } 
  }

  void Push(Edge &e) {
    int amt = int(min(excess[e.from], LL(e.cap - e.flow)));
    if (dist[e.from] <= dist[e.to] || amt == 0) return;
    e.flow += amt;
    G[e.to][e.index].flow -= amt;
    excess[e.to] += amt;    
    excess[e.from] -= amt;
    Enqueue(e.to);
  }
  
  void Gap(int k) {
    for (int v = 0; v < N; v++) {
      if (dist[v] < k) continue;
      count[dist[v]]--;
      dist[v] = max(dist[v], N+1);
      count[dist[v]]++;
      Enqueue(v);
    }
  }

  void Relabel(int v) {
    count[dist[v]]--;
    dist[v] = 2*N;
    for (int i = 0; i < G[v].size(); i++) 
      if (G[v][i].cap - G[v][i].flow > 0)
  dist[v] = min(dist[v], dist[G[v][i].to] + 1);
    count[dist[v]]++;
    Enqueue(v);
  }

  void Discharge(int v) {
    for (int i = 0; excess[v] > 0 && i < G[v].size(); i++) Push(G[v][i]);
    if (excess[v] > 0) {
      if (count[dist[v]] == 1) 
  Gap(dist[v]); 
      else
  Relabel(v);
    }
  }

  LL GetMaxFlow(int s, int t) {
    count[0] = N-1;
    count[N] = 1;
    dist[s] = N;
    active[s] = active[t] = true;
    for (int i = 0; i < G[s].size(); i++) {
      excess[s] += G[s][i].cap;
      Push(G[s][i]);
    }
    
    while (!Q.empty()) {
      int v = Q.front();
      Q.pop();
      active[v] = false;
      Discharge(v);
    }
    
    LL totflow = 0;
    for (int i = 0; i < G[s].size(); i++) totflow += G[s][i].flow;
    return totflow;
  }
};



PushRelabel *dinn;
void add(int a, int b, type c)
{
  dinn->AddEdge(a,b,c);
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);


  PushRelabel din(N);
  dinn = &din;
  int n,m;

  int e = 0;
  cin>>n>>m;
  for(int i=1; i<=n; i++)
    for(int j=1; j<=m; j++)
    {
      cin>>field[i][j];

      if(field[i][j] == -1)
      {
        e = i + j*51 + 2750;
      }
    }

  int s = 2750*2+1;




  for(int i=0; i<=n+2; i++)
  {

    add(s,index(i, 0), infty );
    add(index(i, 0),index(i, 0) + 2750 , infty );
    add(s, index(i, m+1), infty );
    add( index(i, m+1), index(i, m+1) + 2750 , infty );


  }

   for(int i=0; i<=m+2; i++)
  {
    add(s,index(0, i), infty );
    add(index(0, i), index(0, i) + 2750 , infty );

    add(s,index(n+1, i), infty );
    add( index(n+1, i), index(n+1, i) + 2750 , infty );
  }



  for(int i=1; i<=n; i++)
  	for(int j=1; j<=m; j++)
  	{
  		// cout<<"Sdsdd\n";
  		int ind1 = i + j*51;
  		int ind2 = i + j*51 + 2750;

  		if(field[i][j] == -1)
  		{
  			e = ind2;

        add(ind1, ind2, infty);
  		}
  		else
  		{
        add(ind1, ind2, field[i][j]);

        add(ind2,  index(i-1, j), infty );
        add(ind2,  index(i+1, j), infty );
        add(ind2,  index(i, j-1), infty );
        add(ind2,  index(i, j+1), infty );
  		}

      add(index(i-1, j) + 2750,  ind1, infty );
      add(index(i, j-1) + 2750,  ind1, infty );
      add(index(i+1, j) + 2750,  ind1, infty );
      add(index(i, j+1) + 2750,  ind1, infty );

  	}

    type res = din.GetMaxFlow(s,e);
    // type res = find_max_flow();
    if(res >= infty)
    	cout<<"NIE\n";
    else
    	cout<<res<<"\n";

  return 0;
 
}