#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int tab[1033][1033];
int seq[10044];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  	for(int j=0; j<n; j++)
  		cin>>tab[i][j];

  for(int i=0; i<n; i++)
  {
  	int c1 = tab[0][i];
  	int c2 = tab[i][1];
  	int c3 = tab[1][0];

  	if( i == 0)
  	{
  		c1 = tab[2][0];
  		c2 = tab[0][1];
  		c3 = tab[1][2];
  	}

  	if( i == 1)
  	{
  		c1 = tab[0][1];
  		c2 = tab[1][2];
  		c3 = tab[2][0];
  	}
  	seq[i] = (c2 -c3+c1)/2;
  }

  // if(n == 2)
  // {
  // 	// if( s[0][1] == 1)
  // 	// cout<<"0 1\n";
  // 	cout<<"0 0\n";
  // 	return 0;
  // }

  for(int i=0; i<n; i++)
  	cout<<seq[i]<<" ";
  cout<<"\n";


  return 0;
 
}