#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;


bool dfs_visited[100033];
bool low_visited[100033];
bool artpoints[100033];
bool trueartpoints[100033];
bool reache[100033];




int low[100033];

// vector<bool> to_del[100033];
vector<int> graph[100033];
vector<int> graph2[100033];
// vector<int> graph3[100033];

// vector<int> artpoints;

void calculate_low(int d, int v)
{
	int mn = d;

	low_visited[v] = 1;

	low[v] = d;

	bool art  = false;

	for(int i=0; i<graph[v].size(); i++)
	{	
		// cout<<graph[v][i]<<"\n";
		if(graph[v][i] > 0)
		{
			if( !low_visited[graph[v][i]] )
			{
				calculate_low(d+1, graph[v][i]);

				if( low[ graph[v][i]] >= d)
				{
					art = true;
					if(reache[ graph[v][i] ] )
						trueartpoints[v] = 1; 
				}
			}

			mn = min( mn, low[graph[v][i]]);	
		}
	}

	if(art)
	{
		artpoints[v] = 1;
		trueartpoints[v] = 1;
	}

	low[v] = mn;
}

bool dfs(int v, int e)
{
	dfs_visited[v] = 1;

	if(v == e)
	{
		reache[e] = true;
		return true;
	}

	bool rr = false;
	for(int i=0; i<graph2[v].size(); i++)
	{
		bool x = true;
		if(!dfs_visited[graph2[v][i]])
			dfs(graph2[v][i], e);
			if(!reache[graph2[v][i]])
			{
				// cout<<"d: "<<v<<" "<<graph2[v][i]<<"\n";
				// to_del[v][i] = true;
				graph2[v][i] = -1;
				x = false;
				// graph[i][v] = -1;
			}

		if(x)
 			rr = rr || reache[graph2[v][i]];

	}

	if(rr == false)
		for(int i=0; i<graph2[v].size(); i++)
		{
				// cout<<"d: "<<v<<" "<<graph2[v][i]<<"\n";

			graph2[v][i] = -1;
		}

	// if(artpoints[v])
	// 	if(!reache[v])
	// 		trueartpoints[v] = false;
	// 	else
	// 		trueartpoints[v] = true;			
	reache[v] = rr;
	return reache[v];
}



int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int t;
  cin>>t;

  for(int q = 0; q<t; q++)
  {

	  int n, m;
	  int s, e;
	  cin>>n>>m>>s>>e;

	  for(int z = 0; z<n+5; z++)
	  {
		dfs_visited[z] = 0;
		low_visited[z] = 0;
		artpoints[z] = 0;
		trueartpoints[z] = 0;
		reache[z] = 0;
		low[z] = 0;

		graph[z].clear();
		graph2[z].clear();


	  }

	  for(int i=0; i<m; i++)
	  {
	  	int a,b;
	  	cin>>a>>b;

	  	graph2[a].push_back(b);
	  	// to_del[a].push_back(0);
	  }
	  dfs(s, e);

	  for(int i=1; i<=n; i++)
	  {
	  	for(int j=0; j<graph2[i].size(); j++)
	  	{
	  		if(graph2[i][j] > 0 && reache[i] && reache[ graph2[i][j] ])
	  		{
	  			// cout<<i<<" "<<graph2[i][j]	<<"\n";
	  			graph[i].push_back(graph2[i][j]);
	  			graph[graph2[i][j]].push_back(i);
	  		}
	  	}
	  }

	  reache[e] = true;

	  // cout<<reache[s]<<"s\n";
	  calculate_low(1, s);


	  // for(int i=1; i<=n; i++)
	  // {
	  // 	cout<<artpoints[i]<<" "<<reache[i]<<"\n";
	  // 	// cout<<artpoints[i]<<"\n";

	  // }



	  trueartpoints[s] = 1;
	  trueartpoints[e] = 1;
	  int cc = 0;
	  for(int i=1; i<=n; i++)
	  {
	  	if(trueartpoints[i])
	  		cc++;
	  }
	  cout<<cc<<"\n";
	  for(int i=1; i<=n; i++)
	  {
	  	if(trueartpoints[i])
	  		cout<<i<<" ";
	  }
	  cout<<"\n";
  }

  return 0;
 
}