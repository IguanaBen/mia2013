#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

bool solve(type a, type b)
{
  	if(__gcd(a,b) == 1)
  		return 1;

  	type c = __gcd(a,b);
  	while(c % 2 == 0)
  		c /=2;
  	if(c == 1)
  		return 1;
  	else
  		return 0;
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  // for(int i=1; i<10; i++)
  // 	for(int j=1; j<15; j++){
  // 		if(solve(i,j))
  // 			cout<<i<<" "<<j<<"\n";
  // 	}

  // return 0;
  int t;
  cin>>t;
  for(int i=0; i<t; i++)
  {
  	type a,b;
  	cin>>a>>b;

  	if(solve(a,b))
  		cout<<"Y\n";
  	else
  		cout<<"N\n";
  	
  }

  return 0;
 
}