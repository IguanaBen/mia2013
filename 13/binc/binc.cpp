#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;


type zero[50];
type one[50];
type all[50];

bool search(int n, int k)
{
	if(n == 0)
		return true;

	if(k > all[n])
	{
		return false;
	}
	else
	{
		int z =  zero[n];
		int o = one[n];
		if( k > z)
		{
			cout<<"1";
			search(n-1, k-z);
		}
		else
		{
			cout<<"0";
			search(n-1, k);
		}

	}
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  one[1] = 1;
  zero[1] = 1;
  all[1] = 2;
  for(int i=2; i< 50;i++)
  {
  	one[i] = zero[i-1];
  	zero[i] = one[i-1] + zero[i-1];
  	all[i] = one[i] + zero[i];
  }

  // for(int i=1; i< 5;i++)
  // {
  // 	cout<<all[i]<<"\n";
  // }
  int t;
  cin>>t;
  while(t--)
  {
	  type n, k;
	  cin>>n>>k;
	  if(!search(n,k))
	  	cout<<"-1\n";
	  else
	  	cout<<"\n";

  }
  
  return 0;
 
}