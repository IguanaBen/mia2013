#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type tab[1000033];
type dyn[1000033];


const type prime = 1000000009;


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  type d;
  cin>>n>>d;

  for(int i=0; i<n; i++)
  	cin>>tab[i];

  sort(tab, tab+n);

  dyn[n-1] = 1;
  dyn[n] = 1;
  int ptr1 = n-1;
  for(int i=n-2; i>=0; i--)
  {
    while(tab[ptr1]  > tab[i] + d && ptr1 != i)
      ptr1--;
    // cout << i <<" " << tab[ptr1]<<" \n";
    int l = ptr1 - i + 1;
    // cout<<l<<"\n";
    dyn[i] = (dyn[i+1] * l) % prime;
    // cout<<dyn[i]<< " \n" ;
  }
  cout<<dyn[0]<<"\n";

  return 0;
 
}