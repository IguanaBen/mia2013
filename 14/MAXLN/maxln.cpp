#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type t;
  cin>>t;
  for(int i=0; i<t; i++)
  {
  	type n;
  	cin>>n;
  	cout<<"Case "<<i+1<<": "<<4*n*n<<".250000\n";
  }

  return 0;
 
}