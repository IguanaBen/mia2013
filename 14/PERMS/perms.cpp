#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type C[20][20];
type fact[20];

int main()
{

  C[0][0] = 1;
  C[1][0] = 1;
  C[0][1] = 0;
  

  fact[0] = 1;
  for(type i=1; i<20; i++)
  	fact[i] = fact[i-1]*i;

  for(int i=0; i<20; i++)
  {
  	for(int j=0; j<20; j++)
  	{
  		if(j == 0 || j == i)
  			C[i][j] =1;
  		else
  			C[i][j] = C[i-1][j-1] + C[i-1][j];
  	}
  }

   cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  
  // cout<<fact[5]<<"\n
 	
  type n;
  cin>>n;

  type nc = n;

  type res = 1;

  for(int a=0; a<n; a++)
  {
  	type t;
  	cin>>t;
  	for(int b=0; b<t; b++)
  	{
  		res *= C[nc][a+1]*fact[a];
  		nc -= ( a+1);
  	}
  	res /= fact[t];
  }
  cout<<res<<"\n";
  return 0;
 
}