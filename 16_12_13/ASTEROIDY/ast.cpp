#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

pair<type, type> ast[200033];



int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  priority_queue<type> q;

  type stat = 0;

  int n;
  cin>>n;

  int shoots = 0;
  type diff = 0;
  type pt = 0;
  for(int i=0; i<n; i++)
  {
  	type t, d;
  	cin>>t>>d;

  	diff = t - pt;
  	pt = t;

  	stat -=d;
  	stat += diff;

  	q.push(d);

  	while(stat < 0)
  	{
  		type r = q.top();
  		q.pop();
  		stat += r;
  		shoots++;
  	}

  }

  cout<<shoots<<"\n";

  return 0;
 
}



