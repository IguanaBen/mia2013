#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int last_lvl = 262144;

int tab1[524233];
int tab2[524233];

int pos_tab[524233];

int get_sum(int tab[], int elem)
{
	int ind = elem + last_lvl;
	int sum = tab[ind];
	ind /= 2;
	
	while(ind > 1)
	{
		if( ind % 2 )
		{
			sum+= tab[ind*2];
			cout<<ind<<" "<< tab[ind*2]<<"\n";
		}

		// cout<<ind<<"\n";
		ind /=2;
	}

	return sum;
}

void set(int tab[], int elem, int val)
{
	int ind = elem + last_lvl;
	tab[ind] = val;
	ind /=2;
	while(ind > 0)
	{
		tab[ind] = tab[ind*2] + tab[ind*2+1];
		ind /=2;
	}
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  set(tab1, 0, 1);
  set(tab1, 3, 1);
  set(tab1, 7, 1);
  set(tab1, 8, 1);
  cout<<tab1[8]<<"\n";

  cout<<get_sum(tab1, 3)<<"\n";

  return 0;
 
}



