#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type tab1[200033];
type tab2[200033];

type postab1[200033];
type postab2[200033];

type cunt(int beg, int end, int n)
{

	if(beg == end)
	{
		return 0;
	}

	if(beg == end+1)
	{

		if(tab1[beg] == tab2[end] && tab2[end] == tab1[beg])
			return 1;

		return 0;
	}

	int pivot = beg + (end-beg)/2;
	type r1 = cunt(beg, pivot, n);
	type r2 = cunt(pivot + 1, beg, n);

	type s = 0;

	for(int i=1; i<=n; i++)
	{
		int p1 = postab1[i];
		int p2 = postab2[i];

		if(p1 > p2)
			swap(p1, p2);

		if( p1<=pivot && p2 > pivot)
			s++;
	}

	return r1 + r2 + (s)*(s-1)/2;

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  {
  	int a, b;
  	cin>>a>>b;

  	tab1[i] = a;
  	tab2[i] = b;

  	postab1[a] = i;
  	postab2[b] = i;


  }

  cout<<cunt(0, n-1, n)<<"\n";

  return 0;
 
}



