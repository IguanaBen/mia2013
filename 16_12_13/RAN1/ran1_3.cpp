#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int tab1[200033];
int tab2[200033];
int tab3[200033];

pair<int, int> tab[200033];


// type postab1[200033];
// type postab2[200033];

type poss[200033];

long long int inw(int s, int e, int tab[])
{
	// cout<<s<<" "<<e<<endl;
	if(e-s == 0)
		return 0;
	if(e-s == 1)
	{
		if(tab[s] > tab[e])
		{
			swap(tab[s], tab[e]);
			return 1;
		}
		else
			return 0;
	}

	int med = (e-s)/2;
	long long int l1 = inw(s, s + med, tab);
	long long int l2 = inw(s+ med+1, e, tab);
	long long int count = 0;
	long long int res = 0;
	int s_ind  = s + med + 1;

	// cout<<s<< " "<<s+med<<" "  <<s + med + 1 <<" "<<e<<endl;
	// cout << l1<<" "<<l2<<endl;

	for (int i = s; i <= s + med ; ++i)
	{
		/*if(s_ind != e)
		{*/
			for (int j = s_ind; j <= e; ++j)
			{
				
				if(tab[i] > tab[j])
				{
					count++;
					s_ind++;
				}
				else
					break;
			}
		/*}*/
		// else
		// {
			// count++;
		// }

		res += count;
	}


	std::vector<int > vr(e-s+1);
	merge(tab + s, tab + (s + med + 1) , tab + (s + med + 1), tab + (s + med + 1) + (e-s-med-1) +1, vr.begin() );

	for (int i = s; i <= e; ++i)
	{
		tab[i] = vr[i-s];
	}


	return res + l1 + l2;
}

bool cmp(pair<int, int> p1, pair<int, int> p2)
{
	return p1.first < p2.first;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  {
  	int a, b;
  	cin>>a>>b;
  	tab[i].first = a;
  	tab[i].second = b;


  	// tab1[i] = a;
  	// tab2[i] = b;

  	// postab1[a] = i;
  	// postab2[b] = i;
  }

  // for(int i=0; i<n; i++)
  // {
  // 	poss[tab1[i]] = i+1;
  // 	tab1[i] = i+1;
  // }

  // for(int i=0; i<n; i++)
  // {
  // 	tab2[i] = poss[tab2[i]];
  // }

  // for(int i=0; i<n; i++)
  // {
  // 	cout<<tab2[i]<<" ";
  // }

  // for(int i=0; i<n; i++)
  // {
  // 	tab3[tab1[i]-1]=tab2[i]; 
  // }
  sort(tab, tab+n, cmp);
    for(int i=0; i<n; i++)
  {
  	tab2[i] = tab[i].second;
  }
  cout<<inw(0, n-1, tab2)<<"\n";

  return 0;
 
}



