#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

pair<int, int> tab1[200033];
pair<int, int> tab2[200033];
pair<int, int> tab3[200033];

int tabtemp[200033];


long long int inw(int s, int e, int tab[])
{
	// cout<<s<<" "<<e<<endl;
	if(e-s == 0)
		return 0;
	if(e-s == 1)
	{
		if(tab[s] > tab[e])
		{
			swap(tab[s], tab[e]);
			return 1;
		}
		else
			return 0;
	}

	int med = (e-s)/2;
	long long int l1 = inw(s, s + med, tab);
	long long int l2 = inw(s+ med+1, e, tab);
	long long int count = 0;
	long long int res = 0;
	int s_ind  = s + med + 1;

	for (int i = s; i <= s + med ; ++i)
	{
			for (int j = s_ind; j <= e; ++j)
			{
				
				if(tab[i] > tab[j])
				{
					count++;
					s_ind++;
				}
				else
					break;
			}

		res += count;
	}


	std::vector<int > vr(e-s+1);
	merge(tab + s, tab + (s + med + 1) , tab + (s + med + 1), tab + (s + med + 1) + (e-s-med-1) +1, vr.begin() );

	for (int i = s; i <= e; ++i)
	{
		tab[i] = vr[i-s];
	}


	return res + l1 + l2;
}

bool cmp(pair<int, int> p1, pair<int, int> p2)
{
	return p1.first < p2.first;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  {
  	int a, b, c;
  	cin>>a>>b>>c;
  	tab1[i].first = a;
  	tab1[i].second = b;

  	tab2[i].first = a;
  	tab2[i].second = c;

  	tab3[i].first = b;
  	tab3[i].second = c;

  }

  type result = 0;

  sort(tab1, tab1+n, cmp);
  
  for(int i=0; i<n; i++)
  	tabtemp[i] = tab1[i].second;

  result += inw(0, n-1, tabtemp);

  sort(tab2, tab2+n, cmp);
  for(int i=0; i<n; i++)
  	tabtemp[i] = tab2[i].second;

  result += inw(0, n-1, tabtemp);

  sort(tab3, tab3+n, cmp);

  for(int i=0; i<n; i++)
  	tabtemp[i] = tab3[i].second;

  result += inw(0, n-1, tabtemp);


  cout<<result/2<<"\n";

  return 0;
 
}



