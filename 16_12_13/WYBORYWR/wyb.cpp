#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

map<type, type> valmap;

type solve(type n)
{

	if( n == 1)
		return 1;

	if( n == 2)
		return 2;


	map<type, type>::iterator it = valmap.find(n);

	if(it != valmap.end())
		return valmap[n];

	type sq = sqrt(n);

	type res = n/2 + 1;

	for(type i=2; i<=sq; i++)
	{
		if( n % i == 0)
		{
			type d1 = i;
			type d2 = n/i;

			type v1 = solve(d1);
			type np1 = n/d1;
			res = min(res, v1*(np1/2 + 1));


			type v2 = solve(d2);
			type np2 = n/d2;
			res = min(res, v2*(np2/2 + 1));

		}
	}

	valmap[n] = res;

	return res;

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type n;
  cin>>n;
  cout<<solve(n)<<"\n";

  return 0;
 
}



