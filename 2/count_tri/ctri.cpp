#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h> 

#define type long long

using namespace std;

// type big_p = 
// type small_p = 

struct tri
{
	int i;
	int x;
	int y;
};

tri array1[100033];
tri array2[100033];

//1 - up, 0-down, 2-left, 3-right
//4 - indexX, 5 - indexY
int point_nmbr[100033][6];

bool cmpX(tri t1, tri t2)
{
	if(t1.x == t2.x)
		return t1.y < t2.y;
	else
		return t1.x < t2.x;
}

bool cmpY(tri t1, tri t2)
{
	if(t1.y == t2.y)
		return t1.x < t2.x;
	else
		return t1.y < t2.y;
}

vector< pair<int, int> > hashmap[1000033];

int XY[][2] = { {0,2} ,{0,3}, {1,2}, {1,3} };

long mod(long a, long b)
{ 
	return (a%b+b)%b; 
}

bool search_map(int x, int y)
{
	type key = mod( (1000000009*x + y), 1000003);
  	
  	for(int i=0; i<hashmap[key].size(); i++)
  	{
  		if(hashmap[key][i].first == x && hashmap[key][i].second == y)
  			return true;
  	}

	return false;
}



int main()
{	
	cin.tie(NULL);
  	std::ios::sync_with_stdio(false);

  	// XY[0][0] = 

  	int n;
  	cin>>n;

  	for(int j=0; j<n; j++)
  	{
  		int x, y;
  		cin>>x>>y;

  		array1[j].i = j;
  		array1[j].x = x;
  		array1[j].y = y;

  		array2[j].i = j;
  		array2[j].x = x;
  		array2[j].y = y;

  		type key = mod( (1000000009*x + y), 1000003);
  		// type z = (1000000009*x + y);
  		// cout<<z<<endl;
  		// type pr = 1000003;
  		// key = key % pr;
  		// cout<<key<<endl;
  		hashmap[key].push_back( make_pair(x, y) );

  	}

  	sort(array1, array1 + n, cmpX);
  	sort(array2, array2 + n, cmpY);

  	int c_nmbr = 0;
  	int prevX = -1000000000;

  	for(int j=0; j<n; j++)
  	{
  		if(array1[j].x != prevX )
  			c_nmbr = 0;
  		else
  			c_nmbr++;

  		point_nmbr[array1[j].i][4] = j;
  		point_nmbr[array1[j].i][1] = c_nmbr; 
  		prevX = array1[j].x;
  	}

  	c_nmbr = 0;
  	prevX = -1000000000;

  	for(int j=n-1; j >=0 ; j--)
  	{
  		if(array1[j].x != prevX )
  			c_nmbr = 0;
  		else
  			c_nmbr++;

  		// point_nmbr[array1[j].i][4] = j;
  		point_nmbr[array1[j].i][0] = c_nmbr; 
  		prevX = array1[j].x;
  	}

  	c_nmbr = 0;
  	int prevY = -1;

  	for(int j=n-1; j >=0 ; j--)
  	{
  		if(array2[j].y != prevY )
  			c_nmbr = 0;
  		else
  			c_nmbr++;

  		point_nmbr[array2[j].i][5] = j;
  		point_nmbr[array2[j].i][2] = c_nmbr; 
  		prevY = array2[j].y;
  	}

  	c_nmbr = 0;
  	prevY = -1000000000;

  	for(int j=0; j<n; j++)
  	{
  		if(array2[j].y != prevY )
  			c_nmbr = 0;
  		else
  			c_nmbr++;

  		point_nmbr[array2[j].i][3] = c_nmbr; 
  		prevY = array2[j].y;
  	}

  	for(int i=0; i<n; i++)
  	{
  		cout<<point_nmbr[i][0]<<" "<<point_nmbr[i][1]<<" "<<point_nmbr[i][2]<<" "<<point_nmbr[i][3]<<endl;
  	}

  	type res = 0;

  	for(int j=0; j<n; j++)
  	{
  		int id = point_nmbr[j][4];
  		int xp = array1[ind].x;
  		int yp = array1[ind].y;
  		for(int k=0; k<1; k++)
  		{
  			int X = XY[k][0];
  			int Y = XY[k][1];
  			cout<<X<<"|"<<Y<<endl;
  			if(point_nmbr[j][X] < point_nmbr[j][Y])
  			{
  				int ind = point_nmbr[j][4];
  				int X = array1[ind].x;

  				int dv = 0;

  				if( X == 0)
  					dv = 1;
  				else
  					dv = -1;

  				ind += dv;

  				while(array1[ind].x == X)
  				{
  					search_map( array1[ind].y ,X)

  					ind += dv;
  				}


  			}
  			else
  			{
  			}


  		}
  	}

  	cout<<res<<"\n";

	return 0;
}