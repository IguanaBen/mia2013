#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h> 

#define type long long

using namespace std;

// type big_p = 
// type small_p = 

struct tri
{
	int i;
	int x;
	int y;
};

tri array1[100033];
tri array2[100033];

//1 - up, 0-down, 2-left, 3-right
//4 - indexX, 5 - indexY

// 0 -down, 1-left
int point_nmbr[100033][6];


bool cmpX(tri t1, tri t2)
{
	if(t1.x == t2.x)
		return t1.y < t2.y;
	else
		return t1.x < t2.x;
}

bool cmpY(tri t1, tri t2)
{
	if(t1.y == t2.y)
		return t1.x < t2.x;
	else
		return t1.y < t2.y;
}

vector< pair<int, int> > hashmap[1000033];

int points[100033][2];

long mod(long a, long b)
{ 
	return (a%b+b)%b; 
}

bool search_map(int x, int y)
{
	type key = mod( (1000000009*x + y), 1000003);
  	
  	for(int i=0; i<hashmap[key].size(); i++)
  	{
  		if(hashmap[key][i].first == x && hashmap[key][i].second == y)
  			return true;
  	}

	return false;
}

void rotate(int n)
{
  for(int i=0; i<n; i++)
  {
    int t = points[i][0];
    points[i][0] = -points[i][1];
    points[i][1] = t;
  }
}

type solve(int n)
{
  type res = 0;

  for(int i=0; i<1000033;i++)
    hashmap[i].clear();

  for(int j=0; j<n; j++)
  {

    int x = points[j][0];
    int y = points[j][1];

    array1[j].i = j;
    array1[j].x = x;
    array1[j].y = y;

    array2[j].i = j;
    array2[j].x = x;
    array2[j].y = y;

    type key = mod( (1000000009*x + y), 1000003);
    hashmap[key].push_back( make_pair(x, y) );
  }

  sort(array1, array1 + n, cmpX);
  sort(array2, array2 + n, cmpY);

  int c_nmbrX = 0;
  int prevX = -1000000000;
  int c_nmbrY = 0;
  int prevY = -1000000000;

  for(int j=0; j<n; j++)
  {
    if(array1[j].x != prevX )
      c_nmbrX = 0;
    else
      c_nmbrX++;

    point_nmbr[array1[j].i][5] = j;
    point_nmbr[array1[j].i][1] = c_nmbrX; 
    prevX = array1[j].x;



    if(array2[j].y != prevY )
      c_nmbrY = 0;
    else
      c_nmbrY++;

    point_nmbr[array2[j].i][4] = j;
    point_nmbr[array2[j].i][0] = c_nmbrY; 
    prevY = array2[j].y;
  }

  // for(int i=0; i<n;i++)
  //   cout<<point_nmbr[i][0]<<" "<<point_nmbr[i][1]<<endl;

  // cout<<endl;

  // for(int i=0; i<n;i++)
  //   cout<<point_nmbr[i][4]<<" "<<point_nmbr[i][5]<<endl;

  // cout<<endl;

  // for(int i=0; i<n; i++)
  //   cout<<array1[i].x<<" "<<array1[i].y<<endl;

  // cout<<endl;

  // for(int i=0; i<n; i++)
  //   cout<<array2[i].x<<" "<<array2[i].y<<endl;

  // cout<<endl;

  // for(int i=0; i<n; i++)
    // cout<<point_nmbr[i][0]<<" "<<point_nmbr[i][1]<<endl;


  for(int i=0; i<n; i++)
  {

    if(point_nmbr[i][0] < point_nmbr[i][1])
    {
      //X-const
      // cout<<point_nmbr[i][0]<<" "<<point_nmbr[i][1]<<endl;
      // cout<<point_nmbr[i][4]<<" "<<point_nmbr[i][5]<<endl<<endl;

      // int X = array2[point_nmbr[i][5]].y;
      int Y = points[i][1];
      int ind = point_nmbr[i][4];
      ind--;
      // cout<<array2[ind].x<<" "<<Y<<endl;

      while(array2[ind].y == Y && ind >= 0)
      {
      //   // cout<<array2[ind].y<<endl;
      //   cout<<array2[ind].x<<"|"<<array2[ind].y<<endl;
      //   cout<<points[i][0]<<"|"<<points[i][1]<<endl;
      //   cout<<points[i][0]<<"|"<<points[i][1] - (points[i][0] - array2[ind].x)<<endl<<endl;

        // cout<<array2[ind].x <<" "<<array2[ind].y<<endl;
        // cout<<points[i][0]<<","<<points[i][1]<<endl;
        // cout<< points[i][0]<<" "<<points[i][1] -(points[i][0] - array2[ind].x)<<endl<<endl;

        res += search_map( points[i][0], points[i][1] - (points[i][0] - array2[ind].x));

      //   // cout<<points[i][0] - array2[ind].y + points[i][1]<<"|"<< points[i][1]<<endl;
      //   // res += search_map(  points[i][0] - array1[ind].y + points[i][1] , points[i][1]);
        ind--;
      }
    }
    else
    {
      // X -const
      // int X = array1[point_nmbr[i][4]].x;
      int X = points[i][0];

      // cout<<point_nmbr[i][0]<<" "<<point_nmbr[i][1]<<endl;
      // cout<<point_nmbr[i][4]<<" "<<point_nmbr[i][5]<<endl<<endl;
      
      // cout<<Y<<endl;

      int ind = point_nmbr[i][5];
      // cout<<"I: "<<ind<<endl;
      ind--;
      // cout<<array1[ind].x<<" "<<X<<endl;
      while(array1[ind].x == X && ind >= 0 )
      {
      //   cout<<array1[ind].x<<","<<array1[ind].y<<endl;
      //   cout<<points[i][0]<<","<<points[i][1]<<endl;
      //   cout<< points[i][0] - (points[i][1] - array1[ind].y)<<","<< points[i][1]<<endl<<endl;
        // cout<<array1[ind].x <<" "<<array1[ind].y<<endl;
        // cout<<points[i][0]<<","<<points[i][1]<<endl;
        // cout<< points[i][0] -(points[i][1] - array1[ind].y) <<" "<< points[i][1]<<endl<<endl;
        res += search_map( points[i][0] - (points[i][1] - array1[ind].y), points[i][1] );
      //   // cout<<array1[ind].x<<" "<<array1[ind].y<<endl;
      //   // cout<<points[i][0]<<" "<<points[i][1] - array1[ind].x + points[i][0]<<endl;
      //   // res += search_map( points[i][0], points[i][1] - array1[ind].x + points[i][0]);
        ind--;
      }

    }
  }

  return res;
}



int main()
{	
	cin.tie(NULL);
  	std::ios::sync_with_stdio(false);

  	// XY[0][0] = 

  	int n;
  	cin>>n;

  	for(int j=0; j<n; j++)
  	{
  		// int x, y;
  		// cin>>x>>y;
      cin>>points[j][0]>>points[j][1];
  	}

  	type res = 0;
    res += solve(n);

    for(int i=0; i<3;i++)
    {
      rotate(n);
      res += solve(n);
    }

  	cout<<res<<"\n";

	return 0;
}