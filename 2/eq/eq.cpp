#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h> 

#define type long long

using namespace std;


int main()
{	
	cin.tie(NULL);
  	std::ios::sync_with_stdio(false);

	int T;
	cin>>T;

	for(int t=0; t<T; t++)
	{

		type s1 = 0;
		type s2 = 0;
		int n;
		cin>>n;
		for(int i=0; i<n;i++)
		{
			type t;
			cin>>t;
			if(i % 2)
				s2+=t;
			else
				s1+=t;
		}

		if(n % 2)
			cout<<"YES\n";
		else
			if(s1 == s2)
				cout<<"YES\n";
			else
				cout<<"NO\n";
	}


	return 0;
}