#include <iostream>
#include <algorithm>

using namespace std;



int main()
{	

	int T;
	cin>>T;

	for(int t=0; t<T; t++)
	{
		int n, k;
		cin>>n>>k;

		if( k > n)
			cout<<"NIE\n";
		else
		{

			if( n == 3*k)
				cout<<"NIE\n";

			if( n < 3*k)
				cout<<"TAK, ZWIEKSZY SIE\n";
			
			if( n > 3*k)
				cout<<"TAK, ZMNIEJSZY SIE\n";
		}

	}


	return 0;
}