#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h> 

#define type long long

using namespace std;


int main()
{	
	cin.tie(NULL);
  	std::ios::sync_with_stdio(false);

	int T;
	cin>>T;

	for(int t=0; t<T; t++)
	{
		string str1;
		string str2;
		cin>>str1>>str2;

		int gc = __gcd(str1.length(), str2.length());

		string str1p = str1;
		string str2p = str2;

		while(str1p.length() < str2.length())
			str1p += str1.substr(0, gc);

		while(str2p.length() < str1.length())
			str2p += str2.substr(0, gc);

		if(str1p == str2p)
			cout<<"YES\n";
		else
			cout<<"NO\n";

	}


	return 0;
}