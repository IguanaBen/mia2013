#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type unsigned long long

using namespace std;

double vege[1033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  double ratio;
  cin>>ratio;

  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  	cin>>vege[i];

  int min_cuts = 10000;

  for(int i=0; i<n; i++)
  	for(int j=1; j<=500; j++)
  	{

  		double max_size = vege[i]/j;

  		double low = max_size*ratio;
  		// cout<<max_size<<" "<<low<<endl;
  		int cuts = 0;

  		for(int k=0; k<n && cuts < 500; k++)
  		{

  			double c = ceil(vege[k]/max_size);
  				
  			if(c <= vege[k]/low)
  			{
  				c--;
  				cuts += c;
  			}
  			else
  			{
  				cuts = 99999999;
  				break;
  			}

  		}
  		// cout<<endl;

  		min_cuts = min(min_cuts, cuts);

  	}



	// 5/2

	// 1.6|1.6|1.6   	

  	// 0.5 < x/z < 2
  	// x/2 < z < x/0.5
  	// 4 < 14/z < 5
	// 14/5 	< z< 14/4

  	cout<<min_cuts<<"\n";

  return 0;
 
}



