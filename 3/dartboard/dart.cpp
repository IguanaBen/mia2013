#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type unsigned long long

using namespace std;



double vals[10];
double score[] = {50.0, 25.0, 10.5, 31.5 , 10.5, 21.0};

double func(double r, double rp,  double stdev, int i)
{
	return exp((-rp*rp)/(2*stdev*stdev))*score[i] - exp((-r*r)/(2*stdev*stdev))*score[i];
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  cin.precision(15);


  double sum = 0.0;
  double r = 0.0;
  double rp = 0.0;
  for(int i=0; i<6; i++)
  	cin>>vals[i];

  double stdev;
  cin>>stdev;

  r = vals[0];
  sum += func(r, 0,  stdev, 0);
  for(int i=1; i<6;i++)
  {
  	r = vals[i];
  	rp = vals[i-1];
  	sum+= func(r, rp,  stdev, i);
  	// cout<<func(r, rp,  stdev, i)<<" "<<r<<" "<<rp<<endl;

  }
  cout.precision(15);
  cout<<sum<<"\n";


  return 0;
 
}



