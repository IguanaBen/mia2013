#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type unsigned long long

using namespace std;

vector< pair<int, int> > Xpoints[2100];
vector< pair<int, int> > Ypoints[2100];

pair<int, int> points[100033];

bool dsq[100033];

vector<int> conhull;

bool cmp(pair<int, int> p1, pair<int, int> p2)
{
	return p1.first < p2.first;
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;
  for(int i=0; i<n; i++)
  {
  	int x, y;
  	cin>>x>>y;
  	
  	points[i].first = x;
  	points[i].second = y;

  	Xpoints[x+1001].push_back( make_pair(y, i));
  	Ypoints[y+1001].push_back( make_pair(x, i));
  }

  for(int i=0; i<2033; i++)
  {
  	if(Xpoints[i].size() > 2)
  	{

  		sort(Xpoints[i].begin(), Xpoints[i].end(), cmp);
  		for(int j=1; j<Xpoints[i].size()-1; j++ )
  		{
  			dsq[ Xpoints[i][j].second ] = true;
  		}
  	}

  	if(Ypoints[i].size() > 2)
  	{
  		sort(Ypoints[i].begin(), Ypoints[i].end(), cmp);
  		for(int j=1; j<Ypoints[i].size()-1; j++ )
  		{
  			dsq[ Ypoints[i][j].second ] = true;
  		}
  	}
  }

  for(int i=0;i<n;i++)
  	if(!dsq[i])
  		conhull.push_back(i);

  double max_d = 0.0;

  for(int i=0; i<conhull.size(); i++)
  	for(int j=0; j<i; j++)
  	{
  		int xx = (points[conhull[i]].first - points[conhull[j]].first);
  		int yy = (points[conhull[i]].second - points[conhull[j]].second);
  		double d = sqrt( xx*xx + yy*yy  );
  		if(max_d < d)
  			max_d = d;
  	}

  cout.precision(15);
  cout<<max_d<<"\n";


  return 0;
 
}



