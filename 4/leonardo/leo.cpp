#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int tab1[30];
int tab2[30];

char tab_char[30];

void zero_tab()
{

  for(int i=0; i<27; i++)
  {
      tab1[i] = 0;
      tab2[i] = 0;
  }
}

int solve( int beg, int ind )
{

  tab1[ind] = 1;
  if(beg == ind)
    return 1;

  // cout<<tab_char[ind];
  return 1+solve(beg, tab_char[ind]-65);
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  string s;
  // cin>>s;

  for(int i=0; i<n;i++)
  {
  	// cin>>s;

  	for(int j=0; j<26; j++)
  	{
  		cin>>tab_char[j];
      // cout<<tab_char[j]<<endl;
  	}

    zero_tab();

    int num_2 = 0;

    for(int j=0; j<26; j++)
    {
      // cout<<tab_char[j];
      if(tab1[j] == 0)
      {
        int c = solve(j, tab_char[j]-65);

        // if( solve(j, tab_char[j]-65) % 2 == 0)
          // cout<<"s\n";
          // num_2++;
          // tab2[]

        if( c % 2 ==0 )
          tab2[c]++;


      tab1[j] == 1;

      }
        
      // cout<<endl;
    }

    bool ok = true;

    for(int i=0; i<28; i++)
    {
      if( tab2[i] == 0 || tab2[i] % 2 == 0)
      {

      }
      else
        ok = false;
    }

    if(!ok)
      cout<<"No\n";
    else
      cout<<"Yes\n";
  }

  return 0;
 
}



