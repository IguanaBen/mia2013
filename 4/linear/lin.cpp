#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;





bool res(type length, type pos)
{
	if(length == 1)
		return 1;
	else
		if( pos >= (length/2) )
			return res(length/2, pos-length/2);
		else
			return !res(length/2, pos);

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type n,k;
  cin>>n>>k;
  type l=1;

  for(int i=0; i<n;i++)
  	l*=2;

  // cout<<l<<"\n";
  if(res(l,k))
  	cout<<"red"<<"\n";
  else
  	cout<<"blue"<<"\n";


  return 0;
 
}



