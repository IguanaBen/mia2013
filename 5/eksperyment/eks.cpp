#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <cstdio>

#define type long long
#define pInt pair<int, int>
#define pIntB pair<int, bool>


using namespace std;

pIntB temp_tab[200033];
// pIntB temp_tab2[200033];
pInt drops[100033];

bool cmp_pair( pIntB p1, pIntB p2  )
{
	if(p1.first == p2.first)
		return p1.second < p2.second;
	return p1.first < p2.first;
}

bool cmp_sing( pIntB p1, pIntB p2  )
{
	return p1.first < p2.first;
}

bool check(int t, int n, int d)
{
	int elem = 0;
	for(int i=0; i<n; i++)
	{
		if( drops[i].second <= t)
		{

			// int ind1 = 2*elem;
			// int ind2 = 2*elem +1;

			// temp_tab[2*elem].first = min( d, drops[i].first - (t-drops[i].second ));
			// temp_tab[2*elem + 1].first = max( 0, drops[i].first + (t-drops[i].second) );

			temp_tab[2*elem].first = drops[i].first - (t-drops[i].second );			
			temp_tab[2*elem + 1].first = drops[i].first + (t-drops[i].second);

			temp_tab[2*elem].second = 0;
			temp_tab[2*elem+1].second = 1; 
			elem++;
		}
	}


// cout<<elem<<"\n";
	sort(temp_tab, temp_tab + elem*2, cmp_pair);

	int balance = 0;
	int prev = -1000;
	for(int i=0; i<elem*2; i++)
	{	
		// cout<<temp_tab[i].first<<" "<<temp_tab[i].second<<" "<<balance<<"\n";
		if(temp_tab[i].second != 0)
		{
			if( temp_tab[i].first >= d )			
				return true;
			balance--;
		}
		else
		{
			balance++;
		}
		if( balance <= 0 && temp_tab[i].first >= 0 && prev != temp_tab[i].first)
			return false;

		prev = temp_tab[i].first;
	}

	return false;

}

int search(int a, int b, int n, int d)
{
	// cout<<a<<" "<<b<<"\n";
	if(a == b)
		return a;

	if(a == b-1)
	{
		if(check(a, n, d))
			return a;
		
		return b;
	}

	int dv = (b-a)/2;

	if(check(a+dv, n, d))
		return search(a, a + dv, n, d);
	else
		return search(a + dv+1, b, n, d);

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int d, n;
  cin>>d>>n;

  // scanf("%d %d ", &d, &n);
  // int mx = 0;
  int mn = 1000000033;
  for(int i=0; i<n; i++)
  {
  	cin>>drops[i].first>>drops[i].second;
  	// scanf( "%d %d", &drops[i].first, &drops[i].second );
  	// mx = max( drops[i].second, mx );
  	mn = min(drops[i].second, mn);
  }

  // cout<<check(0, n, d)<<"\n";
  // cout<<check(3, n, d)<<"\n";

  // for(int i=0; i<10; i++)
  // {
  // 	cout<<check(i, n, d)<<"\n";
  // }
  // if( d+ mx + 1 > 1000000000)
  	// cout<<search(mn, 1000000000, n, d)<<"\n";	
  // else
  	cout<<search(0, 2000000003, n, d)<<"\n";

  return 0;
 
}



