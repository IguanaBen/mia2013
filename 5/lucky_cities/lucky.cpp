#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

vector<int> graph[100033];
int low[100033];
bool low_visited[100033];

int check_touched[100033];
int color[100033];

int marked[100033];

int checked_lows[100033];


void calculate_low(int d, int v)
{
	int mn = d;

	low_visited[v] = 1;
	// cout<<v<<"\n";

	low[v] = d;

	for(int i=0; i<graph[v].size(); i++)
	{	
		if( !low_visited[graph[v][i]] )
			calculate_low(d+1, graph[v][i]);

		mn = min( mn, low[graph[v][i]]);	
	}

	low[v] = mn;
}

bool check(int c1, int c2, int v, int current_color, int c_low)
{
	check_touched[v] = c_low;

	// cout<<v<<"\n";
	// cout<<low[v]<<"\n";
	// cout<<current_color<<" "<<color[v]<<"\n";

	if( current_color == color[v])
		return false;

	color[v] = current_color;

	if(current_color == c1)
		current_color = c2;
	else
		current_color = c1;

	if(low[v] != c_low) 
	{
		bool res = true;
		int num = 0;
		for(int i=0; i<graph[v].size(); i++)
		{
			// cout<<v<<"\n";
			if(low[graph[v][i]] == c_low)
			{

				if( check_touched[  graph[v][i] ] != c_low )
				{
					// cout<<graph[v][i]<<"\n";
					bool x = check( c1, c2, graph[v][i], current_color, c_low);
					num += x;
					res = res && x; 
				}
				else
				{
					if( color[ graph[v][i] ]  == current_color )
					{
						num++;
					}
					else
						res = false;
				}
			}

		}

		if(num && res)
			return true;
		else
			return false;

	}
	else
	{
		// cout<<v<<" V "<<graph[v].size()<<"\n";
		bool res = true;
		int num = 0;
		for(int i=0; i<graph[v].size(); i++)
		{

			// cout<<v<<" "<<graph[v][i]<<"\n";

			if( check_touched[ graph[v][i] ] != c_low)
			{
				bool x = check( c1, c2, graph[v][i], current_color, c_low);
				num += x;
				res = res && x; 
			}
			else
			{
				if( color[graph[v][i]]  == current_color)
				{
						num++;
				}
				else
					res = false;
			}
		}

		if(num && res)
			return true;
		else
			return false;


	}
}

bool art_point[100033];

void mark(int v, int c)
{
	marked[v] = c;
	for(int i=0; i<graph[v].size(); i++)
	{	
		if( marked[graph[v][i]] != c && low[graph[v][i]] == c)
			mark(graph[v][i], c);

		if( art_point[graph[v][i]] )
			marked[graph[v][i]] = c;
	}
}

bool visit_art[100033];

void art_points(int v, int d)
{
	int mx = -1;
	visit_art[v] = 1;
	for(int i=0; i<graph[v].size(); i++)
	{	
		if( ! visit_art[ graph[v][i] ])
		{
			mx = max(mx, low[graph[v][i]]);
			art_points(graph[v][i], d+1);
		}

	}

	if(mx >= d)
		art_point[v] = 1;


}


void zero(int n)
{
	for(int i=0; i<n+3;i++)
	{	
		visit_art[i] = 0;
		art_point[i] = 0;
		graph[i].clear();
		low[i]=0;
		low_visited[i]=0;

		check_touched[i]=0;
		color[i]=0;

		marked[i]=0;

		checked_lows[i]=0;

	}
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int t;
  cin>>t;

while(t > 0)
{


  int n,m;
  cin>>n>>m;


  zero(n);
  for(int i=0; i<m; i++)
  {
  	int a, b;
  	cin>>a>>b;
  	graph[a].push_back(b);
  	graph[b].push_back(a);
  }


  calculate_low(1, 1);

  // cout<<check(1, 2, 3, 1, 1)<<"\n";
  // cout<<check(5, 6, 3, 1, 1)<<"\n";

	art_points(1, 1);
  int c_color = 3;

  for(int i=1; i<=n; i++)
  {

  	if(!marked[i] && graph[i].size() && !checked_lows[low[i]])
  	{
  		checked_lows[low[i]] = 1;
  		bool b = check(c_color, c_color +1, i, c_color, low[i]);
  		if(!b)
  		{
  			mark(i, low[i]);
  		}

  		// cout<<i<<" "<<b<<"\n";
  	}


  	c_color +=2;
  }

  int res = 0;

  for(int i=0; i<n+4;i++)
  	if(marked[i])
  		res++;
  	// cout<<"\n";
  cout<<res<<"\n";

t--;
}


  // for(int i=1; i<=n; i++)
  // 	cout<<low[i]<<" ";
  // cout<<"\n";



  return 0;
 
}



