#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;


const int last_lvl = 524288;

struct node
{
	type array_max[10];
	type array_min[10];

};

node tree[1048576];

type buffer[20];
type tab[10];
type tab1[10];
type tab2[10];



void update(int index)
{

	merge( tree[index*2].array_max, tree[index*2].array_max + 10,
		tree[index*2+1].array_max, tree[index*2+1].array_max + 10, 
		buffer);

	// if(index == 131073)
	// {
	// 	for(int i=0; i<20; i++)
	// 		cout<<buffer[i]<<" ";
	// 	cout<<"\n";

	// }


	for(int i=0; i<10; i++)
		tree[index].array_max[i] = buffer[i+10];

	// if(index == 131073)
	// {

	// }

	merge( tree[index*2].array_min, tree[index*2].array_min + 10,
		tree[index*2+1].array_min, tree[index*2+1].array_min + 10, 
		buffer);

	for(int i=0; i<10; i++)
		tree[index].array_min[i] = buffer[i];


}


// init -1 dla lisci, ale tutaj nie trzeba...
// chyba trzeba dla min
int init_tree()
{
	int index = last_lvl-1;

	for(int i=0;i<last_lvl;i++)
		for(int j=0; j<10; j++)
		{
			if(tree[last_lvl+i].array_min[j] == 0)
				tree[last_lvl+i].array_min[j] = 1000000033;

			if(tree[last_lvl+i].array_max[j] == 0)
				tree[last_lvl+i].array_max[j] = -1;


		}

	for(int i=0;i<last_lvl;i++)
		for(int j=0; j<10; j++)
		{
			if(tree[i].array_min[j] == 0)
				tree[i].array_min[j] = 1000000033;

			if(tree[i].array_max[j] == 0)
				tree[i].array_max[j] = -1;


		}

	for(int i=index; i>0; i--)
	{
		update(i);
	}
}

void upd_max( type * t1, type * t2, type * tab)
{
	merge(t1, t1 +10, t2, t2+10, buffer);

	// for(int i=0; i<20;i++)
	// 	cout<<buffer[i]<<" ";

	// cout<<"\n";

	for(int i=0;i<10;i++)
		tab[i] = buffer[i+10];
}

void upd_min( type * t1, type * t2, type * tab)
{
	merge(t1, t1 +10, t2, t2+10, buffer);


	// for(int i=0; i<20;i++)
	// 	cout<<buffer[i]<<" ";

	// cout<<"\n";

	for(int i=0;i<10;i++)
		tab[i] = buffer[i];
}

int search_max(int a, int b)
{
	int i1 = a + last_lvl;
	int i2 = b + last_lvl;

	upd_max( tree[i1].array_max, tree[i2].array_max, tab2);

	// for(int i=0; i<10; i++)
	// {
	// 	cout<<tab2[i]<<" ";
	// }

	// cout<<"\n";

	int prev_i1 = i1;
	int prev_i2 = i2;

	i1 /= 2;
	i2 /= 2;

	while( i1 != i2)
	{
		int di1 = i1/2;
		int di2 = i2/2;

		if( !(prev_i1 % 2) )
		{
			// cout<<prev_i1<<"\n";

			// for(int i=0 ;i< 10; i++)
			// 	cout<<tree[prev_i1+1].array_max[i]<<" ";
			// cout<<"\n";

			upd_max(tab2, tree[prev_i1+1].array_max, tab2);
		}

		if( (prev_i2 % 2) )
		{
			// cout<<prev_i2<<"\n";

			upd_max(tab2, tree[prev_i2-1].array_max, tab2);
		}

		prev_i1 = i1;
		prev_i2 = i2;

		i1 = i1/2;
		i2 = i2/2;

		// for(int i=0; i<10; i++)
		// {
		// cout<<tab2[i]<<" ";
		// }
		// cout<<"\n";

	}

}

int search_min(int a, int b)
{
	int i1 = a + last_lvl;
	int i2 = b + last_lvl;

	upd_min( tree[i1].array_min, tree[i2].array_min, tab2);

	// for(int i=0; i<10; i++)
	// {
	// 	cout<<tab2[i]<<" ";
	// }

	// cout<<"\n";

	int prev_i1 = i1;
	int prev_i2 = i2;

	i1 /= 2;
	i2 /= 2;

	while( i1 != i2)
	{
		int di1 = i1/2;
		int di2 = i2/2;


		// cout<<prev_i1<<" "<<prev_i2<<"\n";
		if( prev_i1 % 2 == 0)
		{
			upd_min(tab2, tree[prev_i1+1].array_min, tab2);
		}

		if( (prev_i2 % 2) )
		{
			upd_min(tab2, tree[prev_i2-1].array_min, tab2);
		}

		prev_i1 = i1;
		prev_i2 = i2;

		i1 = i1/2;
		i2 = i2/2;
	}

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n, k, q;
  cin>>n>>k>>q;

  for(int i=0 ;i<n; i++)
  {
  	cin>>tree[last_lvl+i].array_max[9];
  	tree[last_lvl+i].array_min[0] = tree[last_lvl+i].array_max[9];

  	// for(int j=1; j<10; j++)
  	// 	tree[last_lvl+i].array_min[j] = 1000000003;

  	// for(int j=1; j<10; j++)
  	// 	tree[last_lvl+i].array_max[j] = -1;

  }

  init_tree();


  for(int i=0; i<q; i++)
  {
  	int a, b;
  	cin>>a>>b;
  	search_max(a-1, b-1);
  	int s1 = 0;

  	for(int i=0; i<k; i++)
  		s1 += tab2[10-i-1];

  	int s2 = 0;

  	search_min(a-1, b-1);
  	for(int i=0; i<k; i++)
  		 s2+= tab2[i];

  	// cout<<endl;
  	cout<<s1-s2<<"\n";
  	// cout<<s1<<" "<<s2<<" "<<s1-s2<<"\n";

  }


	// 	  							1
	// 	 				 2 	 						               3
	// 		4				            5 					6 		 				7
	// 8			9  	         10  		 11		     12 	13    14   15
   // 16  17  	 18   19      20   21      22   23     24  25  26  27
 	// 0   1      2    3       4    5       6    7     8 

  return 0;
 
}



