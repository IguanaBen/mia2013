#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int tab[5033][5033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n, t;

  cin>>n>>t;

  // for(int i=0; i< 5033; i++)
  // 	for(int j=0; j< 5033; j++)
  // 		tab[i][j] = 0;

  for(int i=0; i<t; i++)
  {
  	int a, b, t;
  	cin>>a>>b>>t;
  	// cin>>tab[a][b];
  	// tab[a][b]++;
  	tab[a][b] = max(tab[a][b], t+1);
  }

  for(int i=1; i<=n; i++)
  {
  	for(int j=1; j<=i; j++)
  	{
  		// cout<<i<<" "<<j<<"\n";
  		tab[i][j] = max(tab[i][j], max( tab[i-1][j-1]-1 , tab[i-1][j]-1 ) );
  	}
  }

  int sum = 0;

  for(int i=1; i<=n; i++)
  {
  	for(int j=1; j<=i; j++)
  	{
  		
  		if(tab[i][j] > 0)
  			// cout<<i<<" "<<j<<" "a<<tab[i][j]<<"\n";
  			sum++;
  	}
  }

  cout<<sum<<"\n";

  return 0;
 
}



