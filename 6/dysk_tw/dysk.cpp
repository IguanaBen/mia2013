#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int root = 1;
int tab[2400000];

int inf = 1000033;

int last_lvl = 524288;

int find_min(int node)
{
	node+=last_lvl;
	int res = inf;

	int ind = node;
	while( ind >=1)
	{
		if( (ind % 2) == 0)
		{
			// int d = ind/2;
			res = min(tab[ind + 1], res);
			// cout<<ind<<" "<<res<<"\n";
		}
		ind/=2;
	}

	return res;

}

int update(int node, int val)
{
	node+=last_lvl;
	tab[node] = val;
	int ind = node;

	ind /= 2;

	while(ind >= 1)
	{
		tab[ind] = min( tab[ind*2], tab[ind*2 + 1] );
		ind /= 2;
	}
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  for(int i=2400000;i >0; i--)
  	tab[i] = inf;
  for(int i=last_lvl; i<last_lvl*3; i++)
  {
  	tab[i] = i-last_lvl;
  }
  // cout<<tab[(last_lvl-1)*2 + 1];
  for(int i=last_lvl-1; i>0; i--)
  {
  	tab[i] = min(tab[i*2], tab[i*2 + 1]);
  }

  // cout<<tab[3]<<"\n";

  for(int a =0; a<n; a++)
  {
  	int t;
  	cin>>t;
  	// cout<<tab[last_lvl + t]<<"\n";
  	update(t, inf);
  	// cout<<tab[(last_lvl + t)/ 4]<<"\n";
  	// cout<<tab[1]<<"\n";
  	int x = find_min(t);
  	if( x <=n)
  		cout<<x<<"\n";
  	else
  		cout<<"NIE\n";
  }


  return 0;
 
}



