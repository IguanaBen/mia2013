#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type qpow(type q, type d, type n)
{
	if( d == 0)
		return 1;

	if(d % 2)
	{
		type t = qpow(q, d/2, n);
		return  (((t*t)% n) * q) % n;
	}
	else
	{
		type t = qpow(q, d/2, n);
		return (t*t) % n;
	}
}

type atab[] = {2, 3, 5, 7, 11, 13, 17};

bool miller_rabin_x(type n, int k)
{
	type s = 0;
	type d = n-1;

	while(d % 2 == 0)
	{
		d = d/2;
		s++;
	}
	int iz = 0;
	for(int i=0; i<k; i++)
	{
		type a = atab[iz];
		iz++;
		// cout<<a<<endl;
		type x = qpow(a, d, n);
		// cout<<a<<" "<<x<<" "<<n-1<<endl;
		if(!( x == 1 || x == n-1))
		{
			bool next = false;
			for(int j=0; j<s; j++)
			{
				x = (x*x) % n;
				if(x == 1)
					return false;

				if(x == n-1)
				{
					next = true;
					break;
				}
			}

			if(!next)
				return false;
		}
		if( a == 17)
			break;

	}

	return true;
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  srand (time(NULL));
  int n_primes = 7;

  for(type i=19; i<=250000000; i+=1 )
  {
  	n_primes += miller_rabin_x(i, 7);
  	// cout<<i% 10000<<"\n";
  	if( (i % 100000) == 0)
  	{
  		cout<<n_primes<<", ";
  		// cout<<i<<", "<<n_primes<<",\n";
  	}

  }

  return 0;
 
}



