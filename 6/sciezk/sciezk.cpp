#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type M0[100][100];
type MK[100][100];
type Mres[100][100];

type M1[100][100];

type Mtemp1[100][100];
type Mtemp2[100][100];
type Mtemp3[100][100];

type prime = 1000000007;

void matrixMult(type M1[100][100], type M2[100][100],type M3[100][100],type n)
{

	for(type i=0; i<n; i++)
		for(type j=0; j<n; j++)
		{
			type r = 0;
			for(type k=0; k<n; k++)
			{
				type a =  (M1[i][k] * M2[k][j]) % prime;
				r += a;
				r = r % prime;
			}

			M3[i][j] = r;
		}
}


void matrixADD(type M1[100][100], type M2[100][100],type M3[100][100],type n)
{

	for(type i=0; i<n; i++)
		for(type j=0; j<n; j++)
		{
			M3[i][j] = (M1[i][j] + M2[i][j]) % prime;
		}
}

void matrixCPY(type M1[100][100], type M2[100][100], type n)
{
	for(type i=0; i<n; i++)
		for(type j=0; j<n; j++)
		{
			M2[i][j] = M1[i][j];
		}
}






void solve(type k, type n)
{
	if(k == 1)
	{
		matrixCPY(M1, MK, n);
		matrixCPY(M1, Mres, n);

		return;
	}
		
	if(k % 2)
	{
		solve(k-1, n);
		
		// matrixADD(MK, M0, Mtemp1, n);
		// matrixMult(Mtemp1, Mres, Mtemp2, n);

		// matrixCPY(Mtemp2, Mres, n);

		// matrixMult(MK, MK, Mtemp1, n);
		// matrixMult(Mtemp1, MK, Mtemp2, n);

		// matrixCPY(Mtemp2, MK, n);


		// matrixADD(MK, Mres, Mtemp1,n);
		// matrixCPY(Mtemp1, Mres,n);

		matrixMult(MK, M1,  Mtemp1, n);
		matrixCPY(Mtemp1, MK, n);

		matrixADD(MK, Mres, Mtemp2, n);
		matrixCPY(Mtemp2, Mres, n);


	}
	else
	{
		solve(k/2, n);
		matrixADD(MK, M0, Mtemp1, n);
		matrixMult(Mtemp1, Mres, Mtemp2, n);

		matrixCPY(Mtemp2, Mres,n);

		matrixMult(MK, MK, Mtemp1,n);
		matrixCPY(Mtemp1, MK,n);

	}
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  for(type i=0; i<100; i++)
  		M0[i][i] = 1;

  type n, m, k;

  cin>>n>>m>>k;
  for(type i=0; i<m; i++)
  {
  	type a,b;
  	cin>>a>>b;
  	a--;
  	b--;
  	M1[a][b] = 1;
  }

  if(n != 1)
  {

  solve(k,n);
  cout<<Mres[0][n-1]<<"\n";

  matrixCPY(M1, Mtemp1, n);
  matrixCPY(M1, Mtemp3, n);
	}
	else
		cout<<1<<"\n";


  // for(int i=0; i<n; i++)
  // 	{
  // 		for(int j=0; j<n; j++)
  // 			cout<<M1[i][j]<<" ";
  // 		cout<<"\n";
  // 	}
  // 	cout<<"\n";

  // for(int i=0 ;i<k-1; i++)
  // {
  // 	matrixMult(Mtemp1, M1, Mtemp2, n);
  // 	matrixCPY(Mtemp2, Mtemp1, n);

  // 	// for(int i=0; i<n; i++)
  // 	// {
  // 	// 	for(int j=0; j<n; j++)
  // 	// 		cout<<Mtemp1[i][j]<<" ";
  // 	// 	cout<<"\n";
  // 	// }
  // 	// cout<<"\n";

  // 	matrixADD(Mtemp1, Mtemp3, Mres, n);
  // 	matrixCPY(Mres, Mtemp3, n);
  // }
  // cout<<Mtemp3[0][n-1]<<"\n";

  return 0;
 
}



