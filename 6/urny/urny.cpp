#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type prime = 1000000007;

type nfact(type n)
{
	if( n == 0)
		return 1;

	type res = 1;
	for(type a=2; a<=n;a++ )
	{		
		res*=a;
		res %= prime;
	}

	return res;
}

type odwr_mod(type a, type b)
{
	type u,w,x,z,q;
	  u = 1; w = a;
	  x = 0; z = b;
	  while(w)
	  {
	    if(w < z)
	    {
	      q = u; u = x; x = q;
	      q = w; w = z; z = q;
	    }
	    q = w / z;
	    u -= q * x;
	    w -= q * z;
	  }
	  if(z == 1)
	  {
	    if(x < 0) x += b;
	   return x;
	  }
	  //nie powinno tak byc
	  return -1;
}

type napr_odwr_mod(type a, type b)
{
	type odwr = odwr_mod(a,b);
	if(odwr < 0 )
	{
			odwr = odwr % b;
			odwr = b + odwr;
	}

	return odwr;
}

type CnK(type n, type k)
{
	// type a1= (n+k-1);
 //  	type a2 = k-1;

	if( k == 0)
		return n;

	type res = nfact(n);
	type x1 = nfact(n-k);
	type x2 = nfact(k);
	type y1 = napr_odwr_mod(x1, prime);
	type y2 = napr_odwr_mod(x2, prime);

	res *= y1;
	res %= prime;
	res *= y2;
	res %= prime;

	return res;

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  // cout<<CnK(1000, 300)<<"\n";
  type n, a, b;
  cin>>n>>a>>b;

  type a1= (n+a-1);
  type a2 = a;

  type b1= (n+b-1);
  type b2 = b;

  type r1 = CnK(a1, a2);
  type r2 = CnK(b1, b2);
  // cout<<r1<<" "<<r2<<"\n";

  type rrr = (r1*r2) % prime;
  cout<<rrr<<"\n";
  

  return 0;
 
}



