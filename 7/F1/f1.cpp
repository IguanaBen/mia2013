#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <iomanip>

#define type unsigned int

using namespace std;

type dp[1004][1004][3][2];
type os[1004][3];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int t;
  cin>>t;
  while(t--)
  {
  	int n;
  	double pp;
  	cin>>n>>pp;

  	type p = round(pp*1000);

  	for(int i=1; i<=n; i++)
  	{
  		double t1, t2;
  		cin>>t1>>t2;
  		os[i][1] = round(t1*1000);
  		os[i][2] = round(t2*1000);
  		// cin>>os[i][1]>>os[i][2];
  	}

  	for(int i=0; i<=n+3; i++)
  		for(int j=0; j<=n+3; j++)
  			for(int k=1; k<=2; k++)
  			{
  				dp[i][j][k][1] = 2147483648;
  				
  				if(i != n+1)
  					dp[i][j][k][0] = 0;

  				if(i != 0)
  					dp[i][n+1][k][0] = 2147483648;

  			}


  	for(int i=1; i<=n; i++)
  	{
  		for(int j=1; j<=n; j++)
  		{
  			for(int k=1; k<=2; k++)
  			{
  				dp[i][j][k][0] = os[j][k] + dp[i-1][j+1][k][0];
  				dp[i][j][k][0] = min(os[j][k] + dp[i-1][1][k][0] + p, dp[i][j][k][0]);

  				dp[i][j][k][1] = os[j][k] + dp[i-1][j+1][k][1];
  				dp[i][j][k][1] = min(os[j][k] + dp[i-1][1][k][1] + p, dp[i][j][k][1]);
  				dp[i][j][k][1] = min(os[j][k] + dp[i-1][1][3-k][1] + p, dp[i][j][k][1]);
  				if( i != 1)
  					dp[i][j][k][1] = min(os[j][k] + dp[i-1][1][3-k][0] + p, dp[i][j][k][1]);
  				// cout<<i<<" "<<j<<" "<<dp[i][j][k][1]<<" "<<os[j][k]<<"|\n";

  			}
  		}
  	}

  	type mn = dp[n][1][1][1];

  	for(int i=1; i<=n; i++)
  	{
  		// cout<<dp[n][i][1][1]<<" "<<dp[n][i][2][1]<<"\n";
  		mn = min( dp[n][i][1][1], mn );
  		mn = min( dp[n][i][2][1], mn );
  	}
  	// cout<<mn<<"\n";
  	if(mn % 1000 < 10)
  		cout<<mn/1000<<".00"<<mn % 1000<<"\n";
  	else
  		if(mn % 1000 < 100)
  			cout<<mn/1000<<".0"<<mn % 1000<<"\n";
  		else
  			cout<<mn/1000<<"."<<mn % 1000<<"\n";

  	// cout<<setiosflags(ios::fixed)<<setprecision(3)<<mn<<"\n";

  }

  return 0;
 
}