#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

pair<type, type> tab[500033];

type d[500033];

bool cmp(pair<type, type> p1, pair<type, type> p2)
{
  return p1.second < p2.second;
}

type mina[500033];
type sumb[500033];
type diff[500033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;

  cin>>n;
  for(int i=0; i<n; i++)
  	cin>>tab[i].first>>tab[i].second;

  sort(tab, tab+n, cmp);

  mina[n-1] = tab[n-1].first;

  for(int i=n-2;i>=0; i--)
    mina[i] = min(mina[i+1], tab[i].first);

  sumb[0] = 0;
  for(int i=1; i<=n; i++)
    sumb[i] = sumb[i-1] + tab[i-1].second;

  diff[0] = 1000000000LL;
  for(int i=1; i<=n; i++)
    diff[i] = min(diff[i-1], tab[i-1].first - tab[i-1].second);

  for(int i=0; i<n; i++)
  {
    // cout<<sumb[i] + mina[i] << " "<<sumb[i+1] + diff[i+1]<<"\n";
    cout<<min(sumb[i] + mina[i], sumb[i+1] + diff[i+1])<<"\n";
  }

  return 0;
 
}



