#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <string.h>
#include <algorithm>

#define type long long

using namespace std;

const int V = 107;
type graph[107][107];

type inf = 10000000;

void clear_graph()
{
	for(int i=0; i<40; i++)
		for(int j=0; j<40; j++)
			graph[i][j] =  0;
}


bool bfs(type rGraph[V][V], int s, int t, int parent[])
{
    // Create a visited array and mark all vertices as not visited
    bool visited[V];
    memset(visited, 0, sizeof(visited));
 
    // Create a queue, enqueue source vertex and mark source vertex
    // as visited
    queue <int> q;
    q.push(s);
    visited[s] = true;
    parent[s] = -1;
 
    // Standard BFS Loop
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
 
        for (int v=0; v<V; v++)
        {
            if (visited[v]==false && rGraph[u][v] > 0)
            {
                q.push(v);
                parent[v] = u;
                visited[v] = true;
            }
        }
    }
 
    // If we reached sink in BFS starting from source, then return
    // true, else false
    return (visited[t] == true);
}
 
// Returns tne maximum flow from s to t in the given graph
int fordFulkerson(type graph[V][V], int s, int t)
{
    int u, v;
 
    // Create a residual graph and fill the residual graph with
    // given capacities in the original graph as residual capacities
    // in residual graph
    type rGraph[V][V]; // Residual graph where rGraph[i][j] indicates 
                     // residual capacity of edge from i to j (if there
                     // is an edge. If rGraph[i][j] is 0, then there is not)  
    for (u = 0; u < V; u++)
        for (v = 0; v < V; v++)
             rGraph[u][v] = graph[u][v];
 
    int parent[V];  // This array is filled by BFS and to store path
 
    type max_flow = 0;  // There is no flow initially
 
    // Augment the flow while tere is path from source to sink
    while (bfs(rGraph, s, t, parent))
    {
        // Find minimum residual capacity of the edhes along the
        // path filled by BFS. Or we can say find the maximum flow
        // through the path found.
        type path_flow = inf*10000;
        for (v=t; v!=s; v=parent[v])
        {
            u = parent[v];
            path_flow = min(path_flow, rGraph[u][v]);
        }
 
        // update residual capacities of the edges and reverse edges
        // along the path
        for (v=t; v != s; v=parent[v])
        {
            u = parent[v];
            rGraph[u][v] -= path_flow;
            rGraph[v][u] += path_flow;
        }
 
        // Add path flow to overall flow
        max_flow += path_flow;
    }
 
    // Return the overall flow
    return max_flow;
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n,m;
  cin>>n>>m;

  while( n!= 0)
  {
  	clear_graph();
  	map<string, int> mp;
  	for(int i=0; i<n; i++)
  	{
  		string str;
  		cin>>str;
  		mp[str] =  i;
  	}

  	for(int i=0; i<m; i++)
  	{
  		string s1, s2;
  		int c1;
  		cin>>s1>>s2>>c1;
  		graph[mp[s1]][mp[s2]] = c1;
  		// graph[mp[s2]][mp[s1]] = c1;

  	}

  	for(int i=0;i<4;i++)
  	{
  		string s1, s2;
  		cin>>s1>>s2;
  		graph[33][mp[s1]] = inf;
  		graph[mp[s2]][34] = inf;
  	}
  	// graph[33][34] = 10;
  	cout<<fordFulkerson(graph, 33, 34) - 8*inf<<"\n";

  	cin>>n>>m;
  }

  return 0;
 
}



