#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
// #include <priority_queue>
#include <algorithm>

#define type long long

using namespace std;


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  priority_queue<type,  std::vector<type>, std::greater<type> > pr;

  int n;
  cin>>n;
  type sum = 0;
  while(n)
  {

  	for(int i=0; i<n; i++)
  	{
	  	type t;
	  	cin>>t;
	  	pr.push(t);
	}

	while(pr.size() > 1 )
	{
		type a = pr.top();
		pr.pop();
		type b = pr.top();
		pr.pop();
		sum += (a+b);
		pr.push(a+b);

	}
	cout<<sum<<"\n";
	sum=0;
	// cout<<pr.top()<<"\n";
	pr.pop();

  	cin>>n;
  }

  // cout<<pr.top();

  return 0;
 
}



