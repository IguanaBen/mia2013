#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type tab[1000033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int t;
  cin>>t;
  while(t--)
  {
  	int n;
  	cin>>n;
  	for(int i=0; i<n; i++) cin>>tab[i];

  	int count = 1;
  	type l = tab[0];

  	for(int i=1; i<n; i++)
  	{
  		if(tab[i] == l)
  			count++;
  		else
  			if( count == 1)
  				l = tab[i];
  			else
  				count--;
  	}
  	// cout<<l<<"\n";
  	int num = 0;
  	int gr = 0;
  	int le = 0;
  	for(int i=0; i<n; i++)
  	{
  		num += tab[i]==l;
  		gr += tab[i] > l;
  		le += tab[i] < l;

  	}

  	if(num > n/2)
  	{
  		if(n % 2)
  		{
  			if(num == (n+1)/2 && (gr == 0 || le == 0) )
  				cout<<"TAK\n";
  			else
  				cout<<"NIE\n";
  		}
  		else
  			cout<<"NIE\n";
  	}
  	else
  		cout<<"TAK\n";

  }

  return 0;
 
}