#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type tab[1000033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type n, m;
  cin>>n>>m;

  tab[0] =1;
  tab[1] =0;

  for(int i=2; i<1000000;i++)
    tab[i] = ((i-1)*(tab[i-1] + tab[i-2])) % m;


  // if(n <= m)
    cout<<((tab[(n % m)] * tab[(n % m)]) % m)<<"\n";
  // else
    // cout<<1<<"\n";

  return 0;
 
}



