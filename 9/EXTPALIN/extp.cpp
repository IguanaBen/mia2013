#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int pi[1000033];


void compute_prefix(string p)
{
  int m = p.length()-1;
  pi[1] = 0;
  int k = 0;
  for(int q = 2; q<=m; q++)
  {
    while( k > 0 && p[k+1]!= p[q])
        k = pi[k];
    if ( p[k+1] == p[q])
      k = k+1;
    pi[q] = k;
  }
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);



  string str1, str;

  while(getline(cin, str1))
  {
  // cin>>str1;


  string str2 = string(str1.rbegin(), str1.rend());
  // str = "#" + str1;

  str =  "."  + str2 + "#" + str1;
  // cout<<str<<"\n";
  compute_prefix(str);

  int n = pi[str.length()-1]; 
  // cout<<n<<"\n";
  cout<<str1 + str2.substr(n, str2.length()-n)<<"\n";
  }

  return 0;
 
}



