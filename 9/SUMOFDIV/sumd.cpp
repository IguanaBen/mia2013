#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

bool primes[1000033];
bool knum[1000033];

int knum_n[1000033];


vector<int> prim;

int main()
{
  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  prim.reserve(100000);
  prim.push_back(2);
  prim.push_back(3);

  for(int i=5; i<1000001; i+=2)
  {
  	bool p = true;
  	int sq = sqrt(i);
  	for(int j=0; j<prim.size() && prim[j] <= sq; j++)
  	{
  		if(i % prim[j] == 0)
  		{
  			p = false;
  			break;
  		}
  	}
  	if(p)
  	{
  		// cout<<i<<"\n";
  		prim.push_back(i);
  	}
  }
  // cout<<prim.size()<<"\n";
  for(int i=0; i<prim.size(); i++)
  	primes[prim[i]] = 1;

  for(int i=0; i<prim.size(); i++)
  {
  	type num = prim[i] + 1;
  	type pw = prim[i];

  	while( num <= 1000000)
  	{
  		// cout<<pw<<"\n";
  		if(primes[num])
  			knum[pw] = true; 
 

  		pw *= prim[i];
  		num += pw;
  	}
  }

  for(int i=1; i < 1000001; i++)
  {
  	if(knum[i])
  		knum_n[i] = knum_n[i-1] + 1;
  	else
  		knum_n[i] = knum_n[i-1];

  }

  // cout<<knum_n[1000000]<<"\n";
  int n;
  cin>>n;

  for(int i=0; i<n; i++)
  {
  	int a,b;
  	cin>>a>>b;

  	cout<<knum_n[b] - knum_n[a-1]<<"\n";

  }

  return 0;
 
}