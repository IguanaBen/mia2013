#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <algorithm>

#define type long long int

using namespace std;

map<type, type> ss;
// map<type, int> mm;


const int PRIME_NUM = 1;

// type primes[PRIME_NUM] = {1000000007, 1000000007};
// type bases[PRIME_NUM] = {10, 11};


type primes[PRIME_NUM] = {1000000007};
type bases[PRIME_NUM] = {10};

type t_hash[1000033][PRIME_NUM];

type ppow[1000033][PRIME_NUM];


type str_hash[10][PRIME_NUM];
int lens[12];


// int perm[4000000][10];


void calculate_hash(string w, int ind)
{
	// w = "maala";
	for(int i=0; i<PRIME_NUM; i++)
	{
		type hash = 0;
		for(int j=0; j< w.length(); j++)
		{
			hash += w[j] - 'a'+1;
			hash *= bases[i];
			hash %= primes[i];
			// cout<<hash<<"\n";
		}
		// cout<<"\n";
		// cout<<"S"<<hash<<"\n";
		str_hash[ind][i] = hash;
	}
}




void permutation(vector<int> &arr, int n)
{
	if(n == 1)
	{
		// add_hash(arr);
		for(int z = 0; z < PRIME_NUM; z++)
		{
			type hash = 0;
			int lsum = 0;

			for(int i=0; i<arr.size(); i++)
			{
				int index = arr[i];
				// hash *= lens[ arr[i-1]];
				hash *= ppow[  lens[ arr[i]] ][z];
				hash += str_hash[ index ][z];
				hash %= primes[z];
				// lsum += lens[ index ];
			}
			// cout<<hash<<"\n\n";
			// mm[hash] = 10;
			ss[hash]++;
		}

		return;
	}
	
	for(int i=0; i<n; i++ )
	{
		permutation(arr, n-1);

		if(n % 2 == 0)
			swap(arr[0], arr[n-1]);
		else
			swap(arr[i], arr[n-1]);
	}
}

void gen_perm(int n)
{	

	type fact = 1;
	for(int i=1; i<=n; i++)
		fact*=n;

	// vector<vector<int> > perm;
	// perm.reserve(fact);
	vector<int> toperm;
	
	for(int i=1; i<=n; i++)
	  	toperm.push_back(i-1);

	permutation(toperm, n);
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  for(int i=0; i< PRIME_NUM; i++)
  {

  	ppow[0][i] = 1;

  	for(int j=1; j<1000033; j++)
  	{
  		ppow[j][i] = ppow[j][i-1] * bases[i];
  		ppow[j][i] %= primes[i];
  	}
  }

  
  int n;
  cin>>n;

  int lsum = 0;

  for(int i=0; i<n; i++)
  {
  	string w;
  	cin>>w;

  	calculate_hash(w, i);
  	lens[i] = w.length();
  	lsum += lens[i];
  	// calculate_hash(w, ind);


  }

  string t;
  cin>>t;
  // cout<<"T "<<t.length()<<"\n";

  // int c=10;
  gen_perm(n);

  type hash = 0;

  for(int i=0; i<lsum; i++)
  {
		hash += t[i] - 'a'+1;
		hash *= bases[0];
		hash %= primes[0];
  }

  type count = 0;
  // cout<<ss.size()<<"\n";

  // cout<<str_hash[0][0]<<"\n";
  // cout<<str_hash[1][0]<<"\n\n";

  	// set<type>::iterator it = ss.begin();
  	// while(it != ss.end())
  	// {
  	// 	cout<<(*it)<<" | \n";
  	// 	it++;
  	// }


  for(int i=0; i < t.length() - lsum + 1; i++)
  {

  	// cout<<hash<<"\n";

  	map<type, type>::iterator it = ss.find(hash);

  	if( it != ss.end())
  		count+=ss[hash];

  	hash -= ppow[ lsum][0] * ( t[i] - 'a' + 1);
  	hash += primes[0];
  	hash %= primes[0];
  	hash += t[i] - 'a' + 1;
  	hash *= bases[0];
  	hash %= primes[0];

  }

  // vector< vector<int> > pp = gen_perm(c);

  // for(int i=0; i<pp.size(); i++)
  // {
  // 	for(int j=0; j<c; j++)
  // 	{
  // 		cout<<pp[i][j]<<" ";
  // 	}
  // 	cout<<"\n";
  // }
  cout<<count<<"\n";
  // string t;

  return 0;
 
}