#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int forest[513][513];
bool visitedforest[513][513];

void resetVisisted(int n)
{
	for(int i=0; i<n+2; i++)
		for(int j=0; j<n+2; j++)
			visitedforest[i][j] = 0;


	for(int i=0; i<=n; i++)
	{
		visitedforest[0][i] = 1;
		visitedforest[i][0] = 1;
		visitedforest[n+1][i] = 1;
		visitedforest[i][n+1] = 1;
	}
}

int cunt(int i, int j, int height)
{
	int res = 1;
	// cout<<i<<" "<<j<<" "<<visitedforest[i][j]<<"\n";
	visitedforest[i][j] = 1;
	if(!visitedforest[i][j+1] && forest[i][j+1] <= height)
		res += cunt(i, j+1, height);

	if(!visitedforest[i+1][j] && forest[i+1][j] <= height)
		res += cunt(i+1, j, height);

	if(!visitedforest[i][j-1] && forest[i][j-1] <= height)
		res += cunt(i, j-1, height);

	if(!visitedforest[i-1][j] && forest[i-1][j] <= height)
		res += cunt(i-1, j, height);

	return res;

}


bool verify(int height, int n, int d)
{
	resetVisisted(n);

	int mx = 0;

	for(int i=1; i<=n; i++)
		for(int j=1; j<=n; j++)
			if(!visitedforest[i][j] && forest[i][j] <= height)
				mx = max(mx, cunt(i,j,height));

	return d <= mx;
}

int bsearch(int b, int e, int n, int d)
{

	// cout<<b<<"\n";
	if(b == e)
		return b;

	if( b == e-1)
	{
		if(verify(b, n, d))
			return b;
		else
			return e;
	}

	int pivot = (e-b)/2 + b;

	if( verify(pivot, n, d) )
		return bsearch(b, pivot, n, d);
	else
		return bsearch(pivot, e, n, d);
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n, d;
  cin>>n>>d;

  for(int i=0; i<n; i++)
	{
		forest[0][i] = 1000000003;
		forest[i][0] = 1000000003;
		forest[n+1][i] = 1000000003;
		forest[i][n+1] = 1000000003;
	}

  int maax= 0;
  for(int i=1; i<=n; i++)
  	for(int j=1; j<=n; j++)
  	{
  		cin>>forest[i][j];
  		maax = max(maax, forest[i][j]);
  	}

  cout<<bsearch(0, maax, n, d)<<"\n";
  // cout<<verify(1, n, 5)<<"\n";



  return 0;
 
}