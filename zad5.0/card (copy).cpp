#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

map< vector<int> , vector<int> > brute_perm;
bool visited[60];


vector<int> multiply(vector<int> v1, vector<int> v2);
void reset_visited()
{
	for(int i=0; i<60; i++)
		visited[i]=0;
}

void cycle(int index, vector<int> perm, int start, vector<int> &cycl)
{
	visited[index] = 1;

	if(perm[index] == start)
		return;

	cycl.push_back(perm[index]);
	cycle(perm[index]-1, perm, start, cycl);
}


bool cmp_cycles(vector<int> c1, vector<int> c2)
{
	return c1.size() > c2.size();
}

vector<int> to_st_perm(vector<int> v)
{
	vector<int> res;
	res.resize(v.size());
	for(int i=0; i<v.size(); i++)
		res[v[i]-1] = i+1;

	return res;
}

void print_perm(vector<int> v)
{
	for(int i=0; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<"\n";
}

struct result_permutation
{
	vector<int> b;

	int parity;
	int casee;
	int pow_cycle;
	int pow_trans;
};

// map<int, int> translation(vector<int> perm)
// {
// 	map<int, int> result;

// 	reset_visited();
// 	vector<int> temp_c;
// 	temp_c.push_back(perm)

// }


// void transposition()
// {

// }


vector< vector<int> > to_cycle(vector<int> perma)
{
	reset_visited();
	vector< vector<int> > cycles;
	for(int i=0; i<perma.size(); i++)
	{
		if(!visited[i])
		{
			visited[i] = 1;
			vector<int> temp_c;
			temp_c.push_back(perma[i]);
			cycle(perma[i]-1, perma, perma[i], temp_c);
			cycles.push_back(temp_c);
		}
	}
	return cycles;
}


vector< pair<int, int> > cycle_to_transpositon(vector<int> cycle)
{
	vector< pair<int, int> > res;
	for(int i=cycle.size()-1; i>0; i--)
		res.push_back(make_pair(cycle[0], cycle[i]));

	return res;
}

result_permutation generate_b_notid(vector<int> perma)
{

	reset_visited();
	vector< vector<int> > cycles;
	for(int i=0; i<perma.size(); i++)
	{
		if(!visited[i])
		{
			visited[i] = 1;
			vector<int> temp_c;
			temp_c.push_back(perma[i]);
			cycle(perma[i]-1, perma, perma[i], temp_c);
			cycles.push_back(temp_c);
		}
	}

	int n_cycles = cycles.size();
	int n_even_cycles = 0;
	int n_5cycles = 0;
	int n_3cycles = 0;
	for(int i=0; i< cycles.size(); i++)
	{
		n_even_cycles += !(cycles[i].size() % 2);

		if( cycles[i].size() > 4)
			n_5cycles++;

		if(cycles[i].size() >  2)
			n_3cycles++;
	}

	// cout<<cycles[0][5]<<"\n";
	// cout<<n_even_cycles<<"\n";

	// tu permb jak f.
	// 0 1 2 3 4..
	// a_1 a_2 ...
	vector<int> b;
	for(int i=0; i< perma.size(); i++)
		b.push_back(i+1);

	int pow_cycle;
	int pow_trans;

	if(n_even_cycles == 0)
	{
		if(n_cycles % 2)
		{
			vector<int>  e;
			bool transp = false;
			for(int i=0; i<cycles.size(); i++)
			{
				if(!transp && cycles[i].size() > 1 )
				{
					b[cycles[i][1]-1] = cycles[i][2];
					b[cycles[i][2]-1] = cycles[i][1];

					transp = true;
				}
				e.push_back(cycles[i][0]);
			}

			for(int i=0; i<e.size()-1; i++)
				b[e[i]-1] = e[i+1];

			b[e[e.size()-1]-1] = e[0];
			
			pow_cycle = e.size()+1;
			pow_trans = e.size();

			result_permutation res;

			res.b = b;
			res.pow_cycle = pow_cycle;
			res.pow_trans = pow_trans;
			res.casee = 0;
			res.parity = 0;

			return res;
		}

		if(n_cycles % 2 == 0)
		{
			sort(cycles.begin(), cycles.end(), cmp_cycles);
			if(n_5cycles)
			{
				vector<int>  e;
				e.push_back(cycles[0][0]);
				int ep =  perma[e[0]-1];
				int tab[2];
				int tt = 0;
				for(int i=1;i<5; i++)
				{
					if(cycles[0][i] != ep)
						tab[tt++] = cycles[0][i];

					if( tt == 2)
						break;
				}

				b[tab[0]-1] = tab[1];
				b[tab[1]] = tab[0];

				for(int i=1; i<cycles.size(); i++)
					e.push_back(cycles[i][0]);

				for(int i=0; i<e.size()-1; i++)
					b[e[i]-1] = e[i+1];

				b[e[e.size()-1]-1] = ep;
				b[ep-1] = e[0];

				pow_cycle = e.size()-1;
				pow_trans = e.size()+0;

				result_permutation res;

				
				res.b = b;
				res.pow_cycle = e.size();
				res.pow_trans = e.size()+1;
				res.casee = 1;
				res.parity = 0;

				return res;

			}
			else
			{
				if(n_3cycles > 1)
				{
					vector<int>  e;
					e.push_back(cycles[0][0]);
					// print_perm(cycles[0]);
					int ep =  perma[e[0]-1];
					// cout<<ep<<" "<<e[0]-1<<"\n";
					if( ep == cycles[0][1] || ep == cycles[0][2])
					{
						b[cycles[1][1]-1] = cycles[1][2];
						b[cycles[1][2]-1] = cycles[1][1];
					}
					else
					{
						b[cycles[0][1]-1] = cycles[0][2];
						b[cycles[0][2]-1] = cycles[0][1];
					}

					for(int i=1; i<cycles.size(); i++)
						e.push_back(cycles[i][0]);

					for(int i=0; i<e.size()-1; i++)
					b[e[i]-1] = e[i+1];

					b[e[e.size()-1]-1] = ep;
					b[ep-1] = e[0];

					pow_cycle = e.size()+1;
					pow_trans = e.size()+0;

					result_permutation res;

					res.b = b;
					res.pow_cycle = e.size();
					res.pow_trans = e.size()+1;
					res.casee = 2;
					res.parity = 0;

					return res;
				}
				else
				{
					vector<int> new_cycle;
					vector<int> temp;
					temp.resize( perma.size()+1, 1 );

					for(int i=0; i<cycles[0].size(); i++)
					{
						temp[cycles[0][i]] = 0;
						new_cycle.push_back(cycles[0][i]);
					}

					for(int i=1; i<=perma.size(); i++)
						if(temp[i])
							new_cycle.push_back(i);

					result_permutation res;

					res.b = b;
					// res.pow_cycle = e.size() + 2;
					// res.pow_trans = e.size() + 1;

					return res;
				}
			}
		}
	}
}

map<int, int> rename_map( vector<int> cycle)
{
	map<int, int> res;
	for(int i=0; i<cycle.size(); i++)
		res[cycle[i]] = i+1;

	return res;
}

vector<int> rename( map<int, int> mm, vector<int> cycle)
{
	vector<int> res;
	for(int i=0; i< cycle.size(); i++)
		res.push_back( mm[cycle[i]] );
	return res;
}


void add_single(vector< pair<int, int> > &transp, pair<int, int> tr)
{

	if( tr.first > tr.second)
		swap(tr.first, tr.second);

	// cout<<"Add: "<<tr.first<<" "<<tr.second<<"\n";
	int d = tr.second - tr.first;

	for(int i=0; i<d-1; i++)
		transp.push_back( make_pair( tr.first+i, tr.first+i+1 ));

	transp.push_back( make_pair(tr.second-1, tr.second));

	for(int i=0; i<d-1; i++)
		transp.push_back( make_pair( tr.second-2-i, tr.second-1-i));
}

void solve_non_id(vector<int> perma, vector<int> permb[], int count)
{
  result_permutation res_perm = generate_b_notid(perma);
  int n = perma.size();
  // return;
  if(res_perm.parity == 0)
  {
  	if(res_perm.casee == 0)
  	{
  		vector<int> st_perm = to_st_perm(res_perm.b);
  		// vector<int> cycle = i
  		vector<int> id; for(int i=0; i<n; i++) id.push_back(i+1);
  		vector<int> cycle = multiply(id, perma);
  		// vector<int> cycle = perma;
  		
  		// permb[0] = multiply(id, permb[0]);

  		for(int i=0; i<res_perm.pow_cycle; i++)
  			cycle = multiply(cycle, st_perm);
  		
  		vector< vector<int> > Cycle = to_cycle(cycle);
  		map<int, int> rmap = rename_map(Cycle[0]);

  		print_perm(st_perm);
  		// vector<int> trans = st_perm;
  		vector<int> trans = multiply(id, st_perm);
  		for(int i=0; i<res_perm.pow_trans-1; i++)
	  			trans = multiply(trans, st_perm);

	  	pair<int, int> Trans;

	  	for(int i=0; i<trans.size(); i++)
	  		if(trans[i] != i+1)
	  		{
	  			Trans.first = i+1;
	  			Trans.second = trans[i];
	  			break;
	  		}

  		vector<int> CycleR = rename(rmap, Cycle[0]);

  		Trans.first = rmap[Trans.first];
  		Trans.second = rmap[Trans.second];


  		if( Trans.first > Trans.second )
  			swap(Trans.first, Trans.second);

  		if( Trans.first + 1< Trans.second)
  			swap(Trans.first, Trans.second);

  		for(int i=0; i<count; i++)
  		{
  			// permb[i] = multiply(id, permb[i]);
  			vector< vector<int> > kcycles = to_cycle(permb[i]);
  			vector< vector<int> > kcyclesRen;


  			for(int j=0; j<kcycles.size(); j++)
  				kcyclesRen.push_back( rename(rmap, kcycles[j]) );


  			vector< pair<int, int>  > allTranspositions;
  			vector<int> protocol;
  			for(int j=0; j<kcyclesRen.size(); j++)
  			{
  				if(kcyclesRen[j].size() > 1)
  				{
  				  vector< pair<int, int> >  transp = cycle_to_transpositon( kcyclesRen[j]);
  				  for(int i=0; i<transp.size(); i++)
  				  	add_single(allTranspositions, transp[i]);

  				}
  			}
  			for(int j=0; j<allTranspositions.size(); j++)
  			{
  				// cout<<"allTranspositions[j]\n";
  				int k = allTranspositions[j].first-Trans.first;
  				// cout<<k<<"\n";
  				if(k < 0)
  					k += n;

  				// if( k != 0)
  				{	
  					for(int z=0; z<k; z++)
  					{

  					protocol.push_back(1);	// ab
  					protocol.push_back(res_perm.pow_cycle);
  					
  					}

  					protocol.push_back(0);
  					protocol.push_back(res_perm.pow_trans);
  					
  					int kn = n-k;
  					// cout<<"kn"<<kn<<"\n";
  					if(kn<0)
  						kn += n;
  					// kn = kn % n;
  					if(kn != 0)
  					{
  						for(int z=0; z<kn; z++)
  						{
  							protocol.push_back(1);	// ab
  							protocol.push_back(res_perm.pow_cycle);
  						}

  					}
  				}
  			}

  			cout<<protocol.size()<<"\n";
  			for(int a=0; a<protocol.size(); a++)
  				cout<<protocol[a]<<" ";
  			cout<<"\n";
  		}
  	}


  	if(res_perm.casee == 1 || res_perm.casee == 2)
  	{
  		cout<<"C: "<<res_perm.casee<<"\n";
  		vector<int> st_perm = to_st_perm(res_perm.b);

  		vector<int> id; for(int i=0; i<n; i++) id.push_back(i+1);
  		// vector<int> cycle = multiply(id, perma);

  		vector< vector<int> > st_cyc = to_cycle(st_perm);

  		int mx = 0;

  		for(int i=0; i<st_cyc.size(); i++)
  		{
  			int ss = st_cyc[i].size();
  			mx = max( ss , mx);
  		}

  		int pw = mx*2 - 1;

  		vector<int> cycle = perma;

  		// print_perm(cycle);

  		for(int i=0; i<res_perm.pow_cycle + 2; i++)
  			cycle = multiply(cycle, st_perm);

	  	vector< vector<int> > Cycle = to_cycle(cycle);

  		int out = 0;

  		map<int, int> rmap;
  		vector<int> tCycle;
  		if(Cycle[0].size() == 1)
  		{
  			out = Cycle[0][0];
  			tCycle = Cycle[1];
  		    rmap = rename_map(Cycle[1]);
  		}
  		else
  		{
  			out = Cycle[1][0];
  			tCycle = Cycle[0];
  			rmap = rename_map(Cycle[0]);
  		}

  		// cout<<"CCC\n";
  		// for(int i=0; i<Cycle.size(); i++)
  		// {
  		// 	// cout<<Cycle[i].size()<<"|\n";
  		// 	print_perm(Cycle[i]);
  		// }
  		// cout<<"CCC\n";


  		// vector<int> small_cycle = st_perm;
  		vector<int> small_cycle = st_perm;

  		for(int i=0; i<res_perm.pow_cycle-1; i++)
  			small_cycle = multiply(small_cycle, st_perm);

  		// cout<<"SMAL\n";
  		// print_perm(small_cycle);

  		vector< vector<int > >sc = to_cycle(small_cycle);
  		vector<int> smallC;
  		for(int i=0; i<sc.size(); i++)
  		{
  			if(sc[i].size() > 1)
  				smallC = sc[i];
  			// print_perm(sc[i]);	
  		}

  		// vector<int> trans = multiply(id, st_perm);
  		vector<int> trans = st_perm;

  		for(int i=0; i<res_perm.pow_trans-1; i++)
	  			trans = multiply(trans, st_perm);

	  	// cout<<"ST: ";
  		print_perm(st_perm);
  		// print_perm(trans);
  		// print_perm(cycle);

  		pair<int, int> Trans;

	  	for(int i=0; i<trans.size(); i++)
	  		if(trans[i] != i+1)
	  		{
	  			Trans.first = i+1;
	  			Trans.second = trans[i];
	  			break;
	  		}
  		
  		
	  	print_perm(tCycle);

  		vector<int> CycleR = rename(rmap, tCycle);

  		Trans.first = rmap[Trans.first];
  		Trans.second = rmap[Trans.second];

  		// cout<<out<<" "<<st_perm[out-1]<<"\n";

  		if( Trans.first > Trans.second )
  			swap(Trans.first, Trans.second);

  		if( Trans.first + 1< Trans.second)
  			swap(Trans.first, Trans.second);

  		for(int i=0; i<count; i++)
  		{

  			vector< vector<int> > kcycles = to_cycle(permb[i]);
  			vector< vector<int> > kcyclesRen;

  			bool is_id = false;
  			int xx = 0;

  			for(int j=0;j<kcycles.size();j++)
  			{
  				for(int l=0; l<kcycles[j].size(); l++)
  				{
  					if(kcycles[j][l] == out)
  					{
  						if(kcycles[j].size() == 1)
  							is_id = true;
  						else
  						{
  							if(l > 0)
  								xx = kcycles[j][l-1];
  							else
  								xx = kcycles[j][ kcycles[j].size() - 1 ];
  						}
  					}
  				}
  			}

	  		vector<int> protocol;

	  		int dxo =0;

  				cout<<"OUT: " <<out<<"\n ";
  				// <<xx<<" "<<sCout<<"\n";

  			if(!is_id)
  			{
  				int sCout = small_cycle[out-1];	//small_cycle(out) = sCout;
  				cout<<"OUT: " <<out<<" "<<xx<<" "<<sCout<<"\n";
  				int pxx = 0;
  				int pout = 0;

  				for(int j=0; j<n-1; j++)
  				{
  					if(tCycle[j] == xx)
	  					pxx = j+1;

	  				if(tCycle[j] == sCout)
	  					pout = j+1;
  				}
  				cout<<pxx<<" "<<pout<<"\n";
  				dxo = pout - pxx;
  				if(dxo < 0)
  					dxo += n-1;


  				cout<<"Dx: "<<dxo<<"\n";
  				// print_perm(small_cycle);
  				// cout<<res_perm.pow_cycle<<"\n";
  				vector<int> tempPerm  = permb[i];
  				print_perm(tempPerm);
  				// vector<int> ida = multiply(id, perma);
  				vector<int> temporaral = cycle;
  				for(int j=0; j<dxo-1; j++)
  				{
  					// tempPerm = multiply(tempPerm, cycle);
  					// temporaral = multiply( temporaral, cycle);

  					tempPerm = multiply(tempPerm, perma);
  					for(int z =0; z< res_perm.pow_cycle + 2 ; z++)
  						tempPerm = multiply(tempPerm, st_perm);
  				}


  				print_perm(tempPerm);


  				for(int z =0; z< res_perm.pow_cycle-1; z++)
  						tempPerm = multiply(tempPerm, st_perm);

  				// tempPerm = multiply(tempPerm, small_cycle);



  				kcycles = to_cycle(tempPerm);
  				cout<<"FIN\n";
  				print_perm(tempPerm);
  				//powrot do pierwotnej


  				vector< vector< int > > cczx = to_cycle( tempPerm );
  				cout<<"_\n";
  				for(int t = 0; t <cczx.size(); t++)
  					print_perm(cczx[t]);
  				cout<<"_\n";

  				cczx = to_cycle(permb[i]);
  				cout<<"_\n";
  				for(int t = 0; t <cczx.size(); t++)
  					print_perm(cczx[t]);
  				cout<<"_\n";

  				// print_perm(tempPerm);


  			}


  			for(int j=0; j<kcycles.size(); j++)
  				kcyclesRen.push_back( rename(rmap, kcycles[j]) );

	  		vector< pair<int, int>  > allTranspositions;
	  		for(int j=0; j<kcyclesRen.size(); j++)
	  		{
	  			if(kcyclesRen[j].size() > 1)
	  			{
	  			  vector< pair<int, int> >  transp = cycle_to_transpositon( kcyclesRen[j]);
	  			  for(int i=0; i<transp.size(); i++)
	  			  	add_single(allTranspositions, transp[i]);
	  			}
	  		}

	  		n = n-1;
	  		for(int j=0; j<allTranspositions.size(); j++)
	  		{
	  			int k = allTranspositions[j].first-Trans.first;
	  			if(k < 0)
	  				k += n;

	  			for(int z=0; z<k; z++)
	  			{
	  				protocol.push_back(1);	// ab
	  				protocol.push_back(res_perm.pow_cycle);
	  			}

	  			protocol.push_back(0);
	  			protocol.push_back(res_perm.pow_trans);
	  					
	  			int kn = n-k;
	  			// cout<<"kn"<<kn<<"\n";
	  			if(kn<0)
	  				kn += n;
	  				// kn = kn % n;
	  			if(kn != 0)
	  			{
	  				for(int z=0; z<kn; z++)
	  				{
	  					protocol.push_back(1);	// ab
	  					protocol.push_back(res_perm.pow_cycle);
	  				}

	  			}
	  		}

	  		// if(!is_id)
	  		// {
	  		// 	//powrot do pierwotnej
  			// 	for(int j=0; j< smallC.size()-1 ; j++)
  			// 	{
  			// 		// tempPerm = multiply(tempPerm, small_cycle);
  			// 	}
  			// 	protocol.push_back(0);
  			// 	protocol.push_back(res_perm.pow_cycle*(smallC.size()-1));
  			// 	// print_perm(tempPerm);

  			// 	for(int j=0; j<n-1 - dxo; j++)
  			// 	{	
  			// 		// tempPerm = multiply(tempPerm, cycle);
  			// 		protocol.push_back(1);
  			// 		protocol.push_back(res_perm.pow_cycle);
  			// 	}
	  		// }
	  		
	  		cout<<protocol.size()<<"\n";
	  		for(int a=0; a<protocol.size(); a++)
	  			cout<<protocol[a]<<" ";
	  		cout<<"\n";

  		}
  	}

  }

}

vector<int> multiply(vector<int> v1, vector<int> v2)
{
	vector<int> res;
	for(int i=0; i<v1.size(); i++) res.push_back(0);
	for(int i=0;i<v1.size(); i++)
		res[v2[i]-1] =  v1[i];
	return res;
}

vector<int> power(vector<int> v1, int a)
{
	if(a == 0)
	{
		vector<int> res;
		for(int i=0; i<v1.size(); i++)
			res.push_back(i+1);
		return res;
	}

	if(a == 1)
		return v1;

	vector<int> res = multiply(v1, v1);

	for(int i=0; i<a-2; i++)
	{
		res = multiply(res, v1);
	}
	return res;
}

type hash_perm(vector<int> v1)
{
	type hash = 0;
	for(int i=0; i < v1.size(); i++)
	{
		hash += v1[i];
		hash *= 10;
	}
}

void permutation(vector< vector<int> > &perms, vector<int> &arr, int n)
{
	if(n == 1)
	{
		perms.push_back(arr);

		return;
	}
	
	for(int i=0; i<n; i++ )
	{
		permutation(perms, arr, n-1);

		if(n % 2 == 0)
			swap(arr[0], arr[n-1]);
		else
			swap(arr[i], arr[n-1]);
	}
}

vector< vector<int> > gen_perm(int n)
{	
	vector< vector<int> > perms;
	vector<int> id;
	for(int i=0; i<n; i++)
		id.push_back(i+1);

	permutation(perms, id, n);
	return perms;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int nn; cin>>nn; vector<int> pp; for(int i=0; i<nn; i++ ) { int t; cin>> t; pp.push_back(t);}
  vector<int> tab[10];
  for(int i=0; i<nn; i++ ) { int t; cin>> t; tab[0].push_back(t);}
  solve_non_id(pp, tab, 1);

  // result_permutation rp= generate_b_notid(pp);
  // cout<<rp.pow_cycle<<"\n";
  // print_perm( vv );
  // vector<int> rr = multiply(pp, vv);
  // rr = multiply(vv, vv);
  // rr = multiply(rr, vv);
  // rr = multiply(rr, vv);
  
  // print_perm( rr);

  return 0;

  int n,k;
  cin>>n;

  vector<int> perm;
  vector<int> k_perms[6];

  for(int i=0; i<n; i++)
  {
  	int t;
  	cin>>t;
  	perm.push_back(t);
  }

  cin>>k;
  for(int i=0; i<k; i++)
  {
  	for(int j=0; j<n; j++)
	  {
	  	int t;
	  	cin>>t;
	  	// cout<<t<<"\n";
	  	k_perms[i].push_back(t);
	  }
  }


  map<vector<int>, vector<int> > brute_perm;

  if(n < 6)
  {

  	map<vector<int>, vector<int> > res_map;

  	int mx = 0;
  	int m_index = 0;

  	vector< vector<int> > perms = gen_perm(n);
  	vector< int > res_perm;

  	 vector< int > id;
  	 for(int j=0; j<n; j++)
  	  	id.push_back(j+1);

  	for(int i=0; i<perms.size(); i++)
  	{

  	  brute_perm.clear();
  	 
  	  brute_perm[id].push_back(0);
  	  // brute_perm[ multiply(id, perm) ].push_back(1);

	  for(int j=0; j<10; j++)
	  {
	  	map<vector<int>, vector<int> > ::iterator  it= brute_perm.begin();

	  	for(;it != brute_perm.end(); it++)
	  	{

		  	vector<int> r1 = multiply(it->first , perm);
		  	vector<int> r2 = multiply(it->first , perms[i]);

		  	if(brute_perm[ r1 ].size() == 0)
		  	{
		  		brute_perm[r1] = it->second;
		  		brute_perm[r1].push_back(1);
		  	}

		  	if(brute_perm[ r2 ].size() == 0)
		  	{
		  		brute_perm[r2] = it->second;
		  		brute_perm[r2].push_back(2);
		  	}
		 }
	  }

	  	int count = 0;
		for(int j=0; j<k; j++)
		{
			if(brute_perm[ k_perms[j] ].size() > 0)
				count++;
		}
		if(count > mx)
		{
			// cout<<count<<"\n";
			mx = count;
			m_index = i;
			res_map = brute_perm;
			res_perm = perms[i];
		}
	}


	for(int i=0; i< res_perm.size(); i++)
		cout<<res_perm[i]<<" ";

	cout<<"\n";

	for(int i=0; i<k; i++)
	{

		if(k_perms[i] == id)
		{
			cout<<"0"<<"\n";
		}
		else
		{
			if( res_map[ k_perms[i] ].size() > 0 )
			{
				// cout<<res_map[k_perms[i]].size()<<" ";
				int s = 0;
				int current = 1;

				vector <int> print_v;


				if(res_map[ k_perms[i]][1] == 2)
				{
					print_v.push_back(0);
					current = 2;
					s++;
					
				}

				int count = 1;
				for(int j=1 + s; j< res_map[ k_perms[i] ].size(); j++)
				{
						bool b = false;
						if(res_map[k_perms[i]][j] == current)
							count++;
						else
						{
							b = true;
							if(current == 2)
								current = 1;
							else
								current = 2;
						}
						print_v.push_back(count);

						if( b )
							count = 1;

				}
				cout<<print_v.size()<<" ";
				for(int it =0; it<print_v.size(); it++)
				{
					cout<<print_v[it]<<" ";
				}
				
				cout<<"\n";
			}
			else
				cout<<"-1\n";
		}
	}

	//wypisz wynik
	//perm id!
  }

  return 0;
 
}