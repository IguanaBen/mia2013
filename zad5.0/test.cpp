#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

map< vector<int> , vector<int> > brute_perm;
bool visited[60];


vector<int> multiply(vector<int> v1, vector<int> v2);


void print_perm(vector<int> v)
{
	for(int i=0; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<"\n";
}

vector<int> multiply(vector<int> v1, vector<int> v2)
{
	vector<int> res;
	for(int i=0; i<v1.size(); i++) res.push_back(0);
	for(int i=0;i<v1.size(); i++)
		res[v2[i]-1] =  v1[i];
	return res;
}

void reset_visited()
{
  for(int i=0; i<60; i++)
    visited[i]=0;
}

void cycle(int index, vector<int> perm, int start, vector<int> &cycl)
{
  visited[index] = 1;

  if(perm[index] == start)
    return;

  cycl.push_back(perm[index]);
  cycle(perm[index]-1, perm, start, cycl);
}

vector< vector<int> > to_cycle(vector<int> perma)
{
  reset_visited();
  vector< vector<int> > cycles;
  for(int i=0; i<perma.size(); i++)
  {
    if(!visited[i])
    {
      visited[i] = 1;
      vector<int> temp_c;
      temp_c.push_back(perma[i]);
      cycle(perma[i]-1, perma, perma[i], temp_c);
      cycles.push_back(temp_c);
    }
  }
  return cycles;
}


bool verify(vector<int> p1, vector<int> p2, vector<int> protocol, vector<int> x)
{

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  int n;
  cin>>n;
  vector<int> p1; for(int i=0; i<n; i++ ) { int t; cin>>t; p1.push_back(t);}
  vector<int> p2; for(int i=0; i<n; i++ ) { int t; cin>>t; p2.push_back(t);}

  print_perm(  multiply(p1,  multiply(p1, p2) ) );

  return 0;
 
}