#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

const int d = 10000;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  srand (time(NULL));

  cout<<d<<"\n";

  for(int i=0; i<d; i++)
  {
  	type x1 = rand() % 1000000;
  	type y1 = rand() % 1000000;
  	double x = double(x1)/10000;
  	double y = double(y1)/10000;

  	cout<<x<<" "<<y<<"\n";
  }

  cout<<0<<"\n";

  return 0;
 
}