#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <iomanip>

#define type double

using namespace std;

typedef pair<type, type> pp;
const type acc = 10000;
const type eps = 1e-10;

type ccw( pp p1, pp p2, pp p3 )
{
	return (p2.first - p1.first)*(p3.second - p1.second) 
			- (p2.second - p1.second)*(p3.first - p1.first);
}

type dist(pp p1, pp p2)
{

	type a = (p1.first - p2.first)* (p1.first - p2.first);
	type b = (p1.second - p2.second)* (p1.second - p2.second);

	return sqrt(a + b);
}


type distance( pp p1, pp p2, pp px )
{
	type area = ccw(p1, p2, px);
	if(area < 0 )
		area = -area;

	return area / dist(p1, p2);
}

struct circle
{
	type a,b,r;
};

circle circle_three_points(pp p1, pp p2, pp p3)
{	
	type a = ( p1.first*p1.first + p1.second*p1.second )*(p2.second - p3.second)
			+ ( p2.first*p2.first + p2.second*p2.second )*(p3.second - p1.second)
			+ ( p3.first*p3.first + p3.second*p3.second )*(p1.second - p2.second);

	type b = ( p1.first*p1.first + p1.second*p1.second )*(p3.first - p2.first)
			+ ( p2.first*p2.first + p2.second*p2.second )*(p1.first - p3.first)
			+ ( p3.first*p3.first + p3.second*p3.second )*(p2.first - p1.first);


	type d = 2.0*(p1.first*(p2.second - p3.second)
				+ p2.first*(p3.second - p1.second)
				+ p3.first*(p1.second - p2.second) );
	a /= d;
	b /= d;

	type r = dist(p1, make_pair(a,b));


	circle res;
	res.a = a;
	res.b = b;
	res.r = r;

	return res;
}

bool check_in_circle(circle c, pp p)
{
	return (dist(p, make_pair(c.a, c.b)) <= (c.r + eps) );
}

bool check(circle c, vector<pp> points)
{
	for(int i=0; i<points.size(); i++)
	{
		if(!check_in_circle(c, points[i]))
			return false;
	}
	return true;
}

circle two_points_known(pp p1, pp p2, vector<pp> points)
{
	circle c0, c1, c2;
	c0.a = (p1.first + p2.first)/2.0;
	c0.b = (p1.second + p2.second)/2.0;
	c0.r = dist(p1, p2)/2.0;

	if(check(c0, points))
		return c0;

	c1.a = 0.0; c1.b = 0.0; c1.r = 100000000.0;
	c2 = c1;

	bool ml, mr;
	ml = false, mr = false;

	circle cl, cr;

	for(int i=0; i<points.size(); i++)
	{
		type cc = ccw(p1, p2 ,points[i]);
		circle ct = circle_three_points(p1, p2, points[i]);
		if(cc)
		{
			if(cc > 0)
			{

				if(!ml)
				{
					cl = ct;
					ml = true;
				}
				else
					if( ccw(p1, p2, make_pair(cl.a, cl.b)) < ccw(p1, p2, make_pair(ct.a ,ct.b)) )
						cl = ct;
			}
			else
			{
				if(!mr)
				{
					cr = ct;
					mr = true;
				}
				else
					if( ccw(p1, p2, make_pair(cr.a, cr.b)) > ccw(p1, p2, make_pair(ct.a ,ct.b)))
						cr = ct;
			}
		}
	}

	if(!ml)
		return cr;

	if(!mr)
		return cl;

	if(cl.r < cr.r)
		return cl;
	return cr;
}

vector<pp> shuffle(vector<pp> vec)
{
	for(int i = 0; i < vec.size(); i++)
	{
        int j = rand() % (i+1);
        swap(vec[i], vec[j]);
    }

    return vec;
}

circle one_known(pp p1, vector<pp> vec)
{
	vector<pp> vec2 = shuffle(vec);

	pp p2 = vec2[0];

	circle c;
	c.a = (p1.first + p2.first)/2.0;
	c.b = (p1.second + p2.second)/2.0;
	c.r = dist(p1, p2)/2.0;

	for(int i=1; i<vec2.size(); i++)
	{
		if( !check_in_circle(c, vec2[i]))
		{
			vector<pp> v(vec2.begin(), vec2.begin() + i );
			c = two_points_known(p1, vec2[i], v);
		}
	}

	return c;

}

circle enc_circle(vector<pp> vec)
{
	vector<pp> vec2 = shuffle(vec);

	pp p1 = vec2[0];
	pp p2 = vec2[1];

	circle c;
	c.a = (p1.first + p2.first)/2.0;
	c.b = (p1.second + p2.second)/2.0;
	c.r = dist(p1, p2)/2.0;

	for(int i=2; i<vec2.size(); i++)
	{
		// cout<<c.a<<" "<<c.b<<" "<<c.r<<" || "<<vec2[i].first<<" "<<vec2[i].second<<"\n";
		if( !check_in_circle(c, vec2[i]))
		{
			vector<pp> v(vec2.begin(), vec2.begin() + i );
			c = one_known(vec2[i], v);
		}
		
	}

		// cout<<c.a<<" "<<c.b<<" "<<c.r<<" || "
		// <<vec2[vec2.size()-1].first<<" "<<vec2[vec2.size()-1].second<<"\n";
	return c;

}

int solve()
{
	int n;
	cin>>n;

	vector<pp> points;
	for(int i=0; i<n; i++)
	{
		type t1, t2;
		cin>>t1>>t2;
		points.push_back(make_pair(t1,t2));
	}

	if(n == 0)
		return 0;

	if(n == 1)
	{
		cout<<setiosflags(ios::fixed)<<setprecision(4)<<points[0].first<<", "<<points[0].second<<"\n";
		return 1;
	}

	if(n == 2)
	{
		type x = (points[0].first + points[1].first)/2.0;
		type y = (points[0].second + points[1].second)/2.0;
		cout<<setiosflags(ios::fixed)<<setprecision(4)<<x<<", "<<y<<"\n";
		return 1;

	}

	// if(n == 3)
	// {
	// 	circle cc = circle_three_points(points[0], points[1], points[2]);
	// 	cout<<setiosflags(ios::fixed)<<setprecision(4)<<cc.a<<", "<<cc.b<<"\n";
	// 	return 1;

	// }

	// circle cc;
	// cc.a = 0.5;
	// cc.b = 0;
	// cc.r = 1;
	// for(int i=0; i<n; i++)
		// cout<<check_in_circle(cc, points[i]);
	// cout<<"\n";
	circle cc = enc_circle(points);
	cout<<setiosflags(ios::fixed)<<setprecision(4)<<cc.a<<", "<<cc.b<<"\n";

	return 1;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  srand (time(NULL));
  // solve();
  while(solve());

  // pp p1 = make_pair(1, 0);
  // pp p2 = make_pair(0, 1);
  // pp p3 = make_pair(0, 0);

  // circle c = circle_three_points(p1, p2, p3);
  // cout<<dist(p1,p2)<<"\n";
  // cout<<c.a<<" "<<c.b<<" "<<c.r<<"\n";

  // cout<<setiosflags(ios::fixed)<<setprecision(4)<<c.a<<" "<<c.b<<" "<<c.r<<"\n";

  return 0;
}