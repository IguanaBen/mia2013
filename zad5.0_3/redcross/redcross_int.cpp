#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <iomanip>

#define type long long

using namespace std;

typedef pair<type, type> pp;
const type acc = 1000000;
const type eps = 0;

type Mul(type a, type b)
{
	return a*b/acc;
}

type Div(type a, type b)
{
	return (a*acc)/b;
}

type ccw( pp p1, pp p2, pp p3 )
{
	return Mul((p2.first - p1.first), (p3.second - p1.second))
			- Mul((p2.second - p1.second),(p3.first - p1.first));
}

type dist(pp p1, pp p2)
{

	type a = Mul(p1.first - p2.first, p1.first - p2.first);
	type b = Mul(p1.second - p2.second, p1.second - p2.second);

	return sqrt(a + b);
}


type distance( pp p1, pp p2, pp px )
{
	type area = ccw(p1, p2, px);
	if(area < 0 )
		area = -area;

	return Div(area , dist(p1, p2));
}

struct circle
{
	type a,b,r;
};

circle circle_three_points(pp p1, pp p2, pp p3)
{	
	type a = Mul( Mul(p1.first, p1.first) + Mul(p1.second,p1.second) , p2.second - p3.second )
			+ Mul( Mul( p2.first, p2.first) + Mul(p2.second,p2.second), p3.second - p1.second)
			+ Mul( Mul(p3.first, p3.first) + Mul(p3.second,p3.second), p1.second - p2.second);

	type b = Mul( Mul(p1.first,p1.first) + Mul(p1.second,p1.second) , p3.first - p2.first)
			+ Mul( Mul( p2.first,p2.first) + Mul(p2.second,p2.second), p1.first - p3.first)
			+ Mul( Mul( p3.first,p3.first) + Mul(p3.second,p3.second), p2.first - p1.first);


	type d = Mul( Mul(p1.first,p2.second - p3.second)
				+ Mul(p2.first, p3.second - p1.second)
				+ Mul(p3.first, p1.second - p2.second),
				2);
	a = Div(a,d);
	b = Div(a,d);

	type r = Mul(Mul(dist(p1,p2), dist(p2,p3)), dist(p3,p1));
	r = Div(r, Mul(2, ccw(p1, p2, p3)) ) ;
	if(r < 0)
		r = -r;


	circle res;
	res.a = a;
	res.b = b;
	res.r = r;

	return res;
}

bool check_in_circle(circle c, pp p)
{
	type aa = p.first - c.a;
	type bb = p.second - c.b;
	return ( (Mul(aa,aa) + Mul(bb,bb)) <= (Mul(c.r,c.r) + eps) );
}

bool check(circle c, vector<pp> points)
{
	for(int i=0; i<points.size(); i++)
	{
		if(!check_in_circle(c, points[i]))
			return false;
	}
	return true;
}

circle two_points_known(pp p1, pp p2, vector<pp> points)
{
	circle c0, c1, c2;
	c0.a = (p1.first + p2.first)/2;
	c0.b = (p1.second + p2.second)/2;
	c0.r = dist(p1, p2)/2;

	if(check(c0, points))
		return c0;

	c1.a = 0; c1.b = 0; c1.r = 1000000000000000000L;
	c2 = c1;

	pp max_l = p1;
	pp max_r = p1;

	for(int i=0; i<points.size(); i++)
	{
		type cc = ccw(p1, p2 ,points[i]);
		if(cc)
		{
			if(cc > 0)
			{
				if(distance(p1, p2, max_l) < distance(p1, p2, points[i]))
					max_l = points[i];
			}
			else
			{
				if(distance(p1, p2, max_r) < distance(p1, p2, points[i]))
					max_r = points[i];	
			}
		}
	}


	if(max_l != p1 && ccw(p1, p2, max_l) != 0)
		c1 = circle_three_points(p1, p2, max_l);
	// else
		// c1 = c0;

	if(max_r != p1 && ccw(p1, p2, max_r) != 0)
		c2 = circle_three_points(p1, p2, max_r);
	// else
		// c2 = c0;

	circle resc;

	// bool bc0 = check(c0, points);
	bool bc1 = check(c1, points);
	bool bc2 = check(c2, points);

	if( bc1 || bc2 )
	{

	}
	else
	{
		// cout<<"IMP\n";
	}

	if(bc1) resc = c1;
	if( (!bc1 && bc2 ) || (bc1 && bc2 && c2.r < c1.r) ) return c2;

	return c1;
}

vector<pp> shuffle(vector<pp> vec)
{
	for(int i = 0; i < vec.size(); i++)
	{
        int j = rand() % (i+1);
        swap(vec[i], vec[j]);
    }

    return vec;
}

circle two_point_circle(pp p1, pp p2)
{
	circle c;
	c.a = Div(p1.first + p2.first, 2);
	c.b = Div(p1.second + p2.second, 2);
	c.r = Div(dist(p1, p2), 2);
	return c;
}

circle one_known(pp p1, vector<pp> vec)
{
	vector<pp> vec2 = shuffle(vec);

	pp p2 = vec2[0];

	circle c = two_point_circle(p1, p2);
	

	for(int i=1; i<vec2.size(); i++)
	{
		if( !check_in_circle(c, vec2[i]))
		{
			vector<pp> v(vec2.begin(), vec2.begin() + i );
			c = two_points_known(p1, vec2[i], v);
		}
	}

	return c;

}

circle enc_circle(vector<pp> vec)
{
	vector<pp> vec2 = shuffle(vec);

	pp p1 = vec2[0];
	pp p2 = vec2[1];

	circle c = two_point_circle(p1, p2);

	for(int i=2; i<vec2.size(); i++)
	{
		cout<<c.a<<" "<<c.b<<" "<<c.r<<" || "<<vec2[i].first<<" "<<vec2[i].second<<"\n";
		if( !check_in_circle(c, vec2[i]))
		{
			// cout<<"NOT\n";
			vector<pp> v(vec2.begin(), vec2.begin() + i );
			c = one_known(vec2[i], v);
		}
	}

	return c;

}

int solve()
{
	int n;
	cin>>n;

	vector<pp> points;
	for(int i=0; i<n; i++)
	{
		long double t1, t2;
		cin>>t1>>t2;

		points.push_back(make_pair( round(t1*acc),round(t2*acc)));
	}

	if(n == 0)
		return 0;

	if(n == 1)
	{
		cout<<setiosflags(ios::fixed)<<setprecision(4)<<points[0].first<<", "<<points[0].second<<"\n";
		return 1;
	}

	if(n == 2)
	{
		type x = Div((points[0].first + points[1].first), 2);
		type y = Div((points[0].second + points[1].second), 2);
		cout<<setiosflags(ios::fixed)<<setprecision(4)<<x<<", "<<y<<"\n";
		return 1;

	}

	// if(n == 3)
	// {
	// 	circle cc = circle_three_points(points[0], points[1], points[2]);
	// 	cout<<setiosflags(ios::fixed)<<setprecision(4)<<cc.a<<", "<<cc.b<<"\n";
	// 	return 1;

	// }

	// circle cc;
	// cc.a = 0.5;
	// cc.b = 0;
	// cc.r = 1;
	// for(int i=0; i<n; i++)
		// cout<<check_in_circle(cc, points[i]);
	// cout<<"\n";
	circle cc = enc_circle(points);
	cout<<setiosflags(ios::fixed)<<setprecision(4)<<cc.a<<", "<<cc.b<<"\n";

	return 1;
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  srand (time(NULL));
  // solve();
  while(solve());

  // pp p1 = make_pair(1, 0);
  // pp p2 = make_pair(0, 1);
  // pp p3 = make_pair(0, 0);

  // circle c = circle_three_points(p1, p2, p3);
  // cout<<dist(p1,p2)<<"\n";
  // cout<<c.a<<" "<<c.b<<" "<<c.r<<"\n";

  // cout<<setiosflags(ios::fixed)<<setprecision(4)<<c.a<<" "<<c.b<<" "<<c.r<<"\n";

  return 0;
}